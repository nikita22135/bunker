import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.12

Page {
    id: root
    anchors.fill: stackView
    visible: false
    property alias backgroundColor: backgroundRect.color
    background: Rectangle{
        id: backgroundRect
        anchors.fill:parent
    }
}
