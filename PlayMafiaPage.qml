import QtQml.Models 2.2
import QtQml 2.2
import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.12

SomePage {
    signal buttonMafiaBackReleased();

    MafiaBackgroundRect {
        id: mafiaPlayBackgroundRect
        anchors.fill: parent
    }

    MafiaConnect {
        id: mafiaConnect
        anchors.centerIn: parent
        width: parent.width/1.3
        height: parent.height/10
        anchors.verticalCenterOffset: -parent.height/8
    }

    MafiaHost {
        id: mafiaHost
        anchors.top: mafiaConnect.bottom
        anchors.left: mafiaConnect.left
        anchors.right: mafiaConnect.right
        anchors.topMargin: mainBunkerPage.buttonMargins
        height: parent.height/10
    }

    ButtonMafiaBack{
        id: mafiaPlayBack
        anchors.top: mafiaHost.bottom
        anchors.left: mafiaHost.left
        anchors.right: mafiaHost.right
        anchors.topMargin: mainBunkerPage.buttonMargins
        height: parent.height/10
        onButtonMafiaBackReleased: {
            playMafiaPage.buttonMafiaBackReleased();
        }
    }
    onButtonMafiaBackReleased: {
        stackView.pop(StackView.Immediate);
    }
}
