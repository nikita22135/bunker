import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.12

Rectangle{
    id: buttonBunker
    signal buttonBunkerReleased();
    color: bunkerButtonMouseArea.containsPress? Qt.darker(mainPage.buttonBunkerColor, 1.1) : mainPage.buttonBunkerColor
    border.width: 2
    border.color: "#333333"
    radius: 15
    Image {
        id: bunkerLogo
        property int iconMargin: 3
        source: "images/icon.png"
        width: parent.height - 3
        height: parent.height - 3
        anchors.left: parent.left
        anchors.leftMargin: iconMargin
        anchors.verticalCenter: parent.verticalCenter
    }
    Text {
        id: bunkerText
        FontLoader{
            id: fontLoader
            source: "fonts/Maler.ttf"
        }
        font.family: fontLoader.name
        
        text: qsTr("Бункер")
        color: "#000000"
        font.pointSize: 18
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.horizontalCenterOffset: 18
     //   font.family: fontLoader2.name
    }
    MouseArea{
        id: bunkerButtonMouseArea
        anchors.fill: parent
        hoverEnabled: true
        onReleased: {
            buttonBunker.buttonBunkerReleased();
        }
    }
}
