import QtQml.Models 2.2
import QtQml 2.2
import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.12

SomePage {
    id: root
    signal backButtonReleased();
    signal bunkerButtonHostReleased();
    signal bunkerConnectButtonReleased();
    Image {
        id: bunkerMultiplayerBackground
        source: "images/mainBackground.jpg"
        anchors.fill: parent
    }
    ButtonPlay{
        id: buttonBunkerConnect
        anchors.centerIn: parent
        width: parent.width/1.3
        height: parent.height/10
        anchors.verticalCenterOffset: -parent.height/8
        buttonText: "Присоединиться"
        onPlayButtonReleased: {
            root.bunkerConnectButtonReleased();
        }
    }

    ButtonBunkerHost {
        id: buttonBunkerHost
        anchors.top: buttonBunkerConnect.bottom
        anchors.left: buttonBunkerConnect.left
        anchors.right: buttonBunkerConnect.right
        anchors.topMargin: mainBunkerPage.buttonMargins
        height: parent.height/10
        onButtonBunkerHostReleased: {
            root.bunkerButtonHostReleased();
        }
    }

    ButtonBack{
        id: buttonBunckerBack
        anchors.top: buttonBunkerHost.bottom
        anchors.left: buttonBunkerHost.left
        anchors.right: buttonBunkerHost.right
        anchors.topMargin: mainBunkerPage.buttonMargins
        height: parent.height/10
        buttonText: "Назад"
        onBackButtonReleased: {
            root.backButtonReleased();
        }
    }

    onBackButtonReleased: {
        stackView.pop(StackView.Immediate);
    }
    onBunkerButtonHostReleased: {
        bunkerLobbyPasswordRect.visible = true;
    }
    onBunkerConnectButtonReleased: {
        client.connectToServer();
        client.writeData("showAvailableBunkerHosts\n");
        stackView.push(bunkerConnectToLobbyPage, StackView.Immediate)
        bunkerConnectToLobbyPage.isTimerRunning = true;
        window.settingsButtonVisibility = false;
        window.soundButtonVisibility = false;
    }

    FooterImage {
        id: multiplayerBunkerFooterImage
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 1
        height: parent.height/12
    }
    Rectangle {
        id: bunkerLobbyPasswordRect
        anchors.fill: parent
        color: Qt.rgba(20,20,20,0.5);
        visible: false
        TextField{
            id: bunkerPassField
            objectName: "bunkerPassField"
            anchors.centerIn: parent
            width: parent.width - 14
            height: 40
            font.pointSize: 14
            color: "#FFCC33"
            background: Rectangle {
                color: bunkerPassField.focus ? "#AAAAAA" : "#515151"
                border.color: "#353535"
                border.width: 1
                radius: 7
            }
        }
        Text {
            id: bunkerPassText
            text: qsTr("Пароль")
            width: bunkerMultiplayerPage.width/3
            height: 40
            font.family: fontLoaderMaler.name
            font.pointSize: 14
            anchors.bottom: bunkerPassField.top
            //                anchors.bottomMargin: 15
            anchors.horizontalCenter: parent.horizontalCenter
        }
        Rectangle {
            id: okButton2
            signal okButton2Released();
            anchors.bottom: parent.bottom
            anchors.horizontalCenter: parent.horizontalCenter
            width: parent.width/1.4
            height: parent.height/10
            anchors.bottomMargin: 7
            color: okButton2MouseArea.containsPress? Qt.darker(mainBunkerPage.buttonColor, 1.1) : mainBunkerPage.buttonColor
            radius: 15
            border.width: 2
            border.color: mainBunkerPage.buttonBorderColor
            Text {
                id: okButton2Text
                font.family: fontLoaderMaler.name
                text: qsTr("ОК")
                anchors.centerIn: parent
                font.pointSize: 14
            }
            MouseArea {
                id: okButton2MouseArea
                anchors.fill: parent
                hoverEnabled: true
                onReleased: {
                    okButton2.okButton2Released();
                }
            }
            onOkButton2Released: {
                bunkerLobbyPasswordRect.visible = false;
                client.connectToServer();
                client.writeData("host lobby\n");
                client.writeData("nicknameHost:"+nickname+"password:"+bunkerPassField.text+"\n");
            }
        }
    }
}
