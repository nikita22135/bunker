import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.12

Rectangle{
    id: infoButton
    signal infoButtonReleased();
    
    color: Qt.rgba(0,0,0,0)
    Image {
        id: infoIcon
        anchors.fill: parent
        source: "images/info.png"
    }
    MouseArea{
        id: infoButtonMouseArea
        anchors.fill: parent
        hoverEnabled: true
        onReleased: {
            infoButton.infoButtonReleased();
        }
    }
}
