import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.12

Rectangle{
    id: settingsButton
    signal settingsButtonReleased();
    property bool isBunkerStyle: true

    color: Qt.rgba(0,0,0,0)
    Image {
        id: settingsIcon
        anchors.fill: parent
        source: isBunkerStyle ? "images/settings.png" : "images/settingsMafia.png"
    }
    MouseArea{
        id: settingsButtonMouseArea
        anchors.fill: parent
        hoverEnabled: true
        onReleased: {
            settingsButton.settingsButtonReleased();
        }
    }
}
