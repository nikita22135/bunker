import QtQml.Models 2.2
import QtQml 2.2
import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.12
import QtGraphicalEffects 1.0

Window {
    id: window
    visible: true
    width: 240
    height: 460
    title: qsTr("Bunker")
    property variant cardText: ["","","","","","","",""]
    property variant nicknames: ["","","","","","","","","","","","","","","",""]
    property variant biology: ["","","","","","","","","","","","","","","",""]
    property variant profession: ["","","","","","","","","","","","","","","",""]
    property variant health: ["","","","","","","","","","","","","","","",""]
    property variant charater: ["","","","","","","","","","","","","","","",""]
    property variant hobbie: ["","","","","","","","","","","","","","","",""]
    property variant phobia: ["","","","","","","","","","","","","","","",""]
    property variant bag: ["","","","","","","","","","","","","","","",""]
    property variant special: ["","","","","","","","","","","","","","","",""]
    property variant bunkerHostsArr: ["","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""]
    property variant bunkerHostsIdArr: [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
    property variant bunkerPlayersCountList: [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
    property variant bunkerIsPassList: [false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false]
    property string catastropheText: ""
    property string nickname: settings.getNickname();
    property int myId: 0
    property alias settingsButtonVisibility: settingsButton.visible
    property int selectedAvatar: settings.getAvatarNumber()
    property alias soundButtonVisibility: soundButton.visible
    signal nicknamesLstChanged();
    signal bunkerHostsChanged();
    signal bunkerHostsIdsChanged();
    function onNicknamesUpdated(nicknamesList){
        nicknames = nicknamesList;
        window.nicknamesLstChanged();
    }
    function onBunkerHostsListUpdated(bunkerHosts){
        bunkerHostsArr = bunkerHosts;
        window.bunkerHostsChanged();
    }
    function onBunkerHostsIdsListUpdated(bunkerHostsIds){
        bunkerHostsIdArr = bunkerHostsIds;
    }
    function onBunkerPlayersCountListUpdated(bunkerPlayersCount){
        bunkerPlayersCountList = bunkerPlayersCount;
    }
    function onBunkerIsPassListUpdated(bunkerIsPass){
        bunkerIsPassList = bunkerIsPass;
    }

    function setId(){
        myId = client.getId();
        gen.setId(myId);
    }

    onNicknamesLstChanged: {
        bunkerLobbyPage.nicknamesChanged();
    }

    onBunkerHostsChanged: {
        bunkerConnectToLobbyPage.bunkerHostsChanged();
    }
    ButtonSettings {
        id: settingsButton
        visible: true
        width: 33
        height: 33
        z: 10
        anchors.top: parent.top
        anchors.topMargin: 6
        anchors.right: soundButton.left
        anchors.rightMargin: 6
        onSettingsButtonReleased: {
            stackView.push(settingsPage, StackView.Immediate);
            window.settingsButtonVisibility = false;
        }
    }
    SoundButton {
        id: soundButton
        visible: true
        width: 33
        height: 33
        z: 10
        anchors.top: parent.top
        anchors.topMargin: 6
        anchors.right: parent.right
        anchors.rightMargin: 6
    }

    Connections{
        target: client
        onHostBunkerApproved:{
            stackView.push(bunkerLobbyPage, StackView.Immediate);
            bunkerLobbyPage.startGameBunkerButtonVisibility = true;
            settingsButtonVisibility = false;
            soundButtonVisibility = false;
        }
        onNicknamesListUpdated:{
            onNicknamesUpdated(nicknamesList);
        }
        onBunkerLobbyHostsListUpdated:{
            onBunkerHostsListUpdated(bunkerHosts);
        }
        onBunkerLobbyHostsIdsListUpdated:{
            onBunkerHostsIdsListUpdated(bunkerHostsIds);
        }
        onBunkerPlayersCountUpdated:{
            onBunkerPlayersCountListUpdated(bunkerPlayersCount);
        }
        onBunkerIsPassUpdated:{
            onBunkerIsPassListUpdated(bunkerIsPass);
        }
        onIdCreated:{
            setId();
        }
        onConnectionToLobbyApproved:{
            stackView.push(bunkerLobbyPage, StackView.Immediate);
            bunkerLobbyPage.startGameBunkerButtonVisibility = false;
        }
        onGameIsNotStarted:{
            bunkerLobbyPage.tooFewPlayersToStartBunkerGameMessageVisibility = true;
        }
        onStartBunkerGame:{
            window.cardText = gen.generateCard();
            // window.catastropheText = gen.generateCatastrophe(); //это надо переписать, пока пусть будет так
            stackView.push(bunkerGamePage, StackView.Immediate);
            bunkerGamePage.updatePlayersInfo();
            bunkerGamePage.setNicknames();
        }
        onNotEnoughSpace:{
            bunkerConnectToLobbyPage.tooManyPlayersToConnectMessageVisibility = true;
        }
        onPassIsNotMatch:{
            bunkerPasswordIsNotMatch.visible = true;
        }
        onBiologyComes:{
            biology[num] = bio;
            bunkerGamePage.updatePlayersInfo();
        }
        onProfessionComes:{
            profession[num] = prof;
            bunkerGamePage.updatePlayersInfo();
        }
        onHealthComes:{
            health[num] = heal;
            bunkerGamePage.updatePlayersInfo();
        }
        onCharacterComes:{
            charater[num] = charact;
            bunkerGamePage.updatePlayersInfo();
        }
        onHobbieComes:{
            hobbie[num] = hob;
            bunkerGamePage.updatePlayersInfo();
        }
        onPhobiaComes:{
            phobia[num] = phob;
            bunkerGamePage.updatePlayersInfo();
        }
        onBagComes:{
            bag[num] = bg;
            bunkerGamePage.updatePlayersInfo();
        }
        onSpecial1Comes:{
            special[num] = spcial1;
            bunkerGamePage.updatePlayersInfo();
        }
    }
    Connections {
        target: gen
        onCatastropheByParametersGenerated:{
            window.catastropheText = catasropheLine;
            bunkerGamePage.update();
        }
        onNewHealthGenerated:{
            window.cardText[2] = newHealth;
           // window.health[playerNum] = newHealth;
          //  bunkerGamePage.updatePlayersInfo();
            bunkerGamePage.healthText = newHealth;
        }
    }

    FontLoader{
        id: fontLoaderMaler
        source: "/fonts/Maler.ttf"
    }

    StackView{
        id: stackView
        initialItem: mainPage
        anchors.fill: parent
    }

    MainPage { id: mainPage }
    SettingsPage { id: settingsPage }
    MainBunkerPage { id: mainBunkerPage }
    MainMafiaPage { id: mainMafiaPage }
    BunkerRulesPage { id: bunkerRulesPage }
    MafiaRulesPage { id: mafiaRulesPage }
    PlayBunkerPage { id: playBunkerPage }
    PlayMafiaPage { id: playMafiaPage}
    BunkerSoloGamePage { id: bunkerSoloGamePage }
    BunkerMultiplayerPage { id: bunkerMultiplayerPage }
    BunkerSoloCardPage { id: bunkerSoloCardPage }
    BunkerSoloCatastrophePage { id: bunkerSoloCatastrophePage }
    BunkerLobbyPage { id: bunkerLobbyPage }
    BunkerConnectToLobbyPage { id: bunkerConnectToLobbyPage }
    BunkerGamePage { id: bunkerGamePage }
}
