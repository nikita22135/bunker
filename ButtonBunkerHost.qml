import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.12

Rectangle{
    id: buttonBunkerHost
    color: buttonHostBunkerMouseArea.containsPress? Qt.darker(mainBunkerPage.buttonColor, 1.1) : mainBunkerPage.buttonColor
    radius: 15
    border.width: 2
    border.color: mainBunkerPage.buttonBorderColor
    property alias buttonText: hostText.text
    signal buttonBunkerHostReleased();
    Text {
        id: hostText
        FontLoader{
            id: fontLoader
            source: "fonts/Maler.ttf"
        }
        font.family: fontLoader.name
        
        text: qsTr("Создать лобби")
        anchors.centerIn: parent
        font.letterSpacing: 0.15
        font.pointSize: 14
    }
    MouseArea{
        id: buttonHostBunkerMouseArea
        anchors.fill: parent
        hoverEnabled: true
        onReleased: {
            buttonBunkerHost.buttonBunkerHostReleased();
        }
    }
}
