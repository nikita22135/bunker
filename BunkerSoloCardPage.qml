import QtQml.Models 2.2
import QtQml 2.2
import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.12

SomePage {
    Image {
        id: bunkerSoloCardBackground
        source: "images/mainBackground.jpg"
        anchors.fill: parent
    }
    Rectangle{
        id: biologyRect
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.topMargin: 3
        height: parent.height/9
        color: "#515151"
        border.width: 1
        border.color: "#202020"
        radius: 5

        Rectangle{
            id: r1
            anchors.left: parent.left
            anchors.leftMargin: 3
            anchors.top: parent.top
            anchors.topMargin: 3
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 3
            width: r1.height
            radius: r1.height/2
            border.color: "#202020"
            border.width: 1
            color: "#909090"
            Image {
                id: i1
                source: "images/sexIcon.png"
                anchors.fill: parent
                anchors.margins: 7
            }
        }
        Text {
            id: t1
            text: window.cardText[0]
            font.family: fontLoaderMaler.name
            anchors.left: r1.right
            anchors.leftMargin: 8
            anchors.right: parent.right
            anchors.rightMargin: 6
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            font.letterSpacing: 0.2
            color: "#FFCC33"
            wrapMode: Text.WordWrap
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignLeft
            font.pointSize: 12
        }
    }

    Rectangle {
        id: professionRect
        anchors.top: biologyRect.bottom
        anchors.topMargin: 2
        anchors.left: parent.left
        anchors.right: parent.right
        height: parent.height/9
        color: "#515151"
        border.width: 1
        border.color: "#202020"
        radius: 5

        Rectangle{
            id: r2
            anchors.left: parent.left
            anchors.leftMargin: 3
            anchors.top: parent.top
            anchors.topMargin: 3
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 3
            width: r2.height
            radius: r2.height/2
            border.color: "#202020"
            border.width: 1
            color: "#909090"
            Image {
                id: i2
                source: "images/professionIcon.png"
                anchors.fill: parent
                anchors.margins: 7
            }
        }
        Text {
            id: t2
            text: window.cardText[1]
            font.family: fontLoaderMaler.name
            anchors.left: r2.right
            anchors.leftMargin: 8
            anchors.right: parent.right
            anchors.rightMargin: 6
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            font.letterSpacing: 0.2
            color: "#FFCC33"
            wrapMode: Text.WordWrap
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignLeft
            font.pointSize: 12
        }
    }

    Rectangle {
        id: healthRect
        anchors.top: professionRect.bottom
        anchors.topMargin: 2
        anchors.left: parent.left
        anchors.right: parent.right
        height: parent.height/9
        color: "#515151"
        border.width: 1
        border.color: "#202020"
        radius: 5

        Rectangle{
            id: r3
            anchors.left: parent.left
            anchors.leftMargin: 3
            anchors.top: parent.top
            anchors.topMargin: 3
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 3
            width: r3.height
            radius: r3.height/2
            border.color: "#202020"
            border.width: 1
            color: "#909090"
            Image {
                id: i3
                source: "images/healthIcon.png"
                anchors.fill: parent
                anchors.margins: 7
            }
        }
        Text {
            id: t3
            text: window.cardText[2]
            font.family: fontLoaderMaler.name
            anchors.left: r3.right
            anchors.leftMargin: 8
            anchors.right: parent.right
            anchors.rightMargin: 6
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            font.letterSpacing: 0.2
            color: "#FFCC33"
            wrapMode: Text.WordWrap
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignLeft
            font.pointSize: 12
        }
    }

    Rectangle {
        id: characterRect
        anchors.top: healthRect.bottom
        anchors.topMargin: 2
        anchors.left: parent.left
        anchors.right: parent.right
        height: parent.height/9
        color: "#515151"
        border.width: 1
        border.color: "#202020"
        radius: 5

        Rectangle{
            id: r4
            anchors.left: parent.left
            anchors.leftMargin: 3
            anchors.top: parent.top
            anchors.topMargin: 3
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 3
            width: r4.height
            radius: r4.height/2
            border.color: "#202020"
            border.width: 1
            color: "#909090"
            Image {
                id: i4
                source: "images/characterIcon.png"
                anchors.fill: parent
                anchors.margins: 7
            }
        }
        Text {
            id: t4
            text: window.cardText[3]
            font.family: fontLoaderMaler.name
            anchors.left: r4.right
            anchors.leftMargin: 8
            anchors.right: parent.right
            anchors.rightMargin: 6
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            font.letterSpacing: 0.2
            color: "#FFCC33"
            wrapMode: Text.WordWrap
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignLeft
            font.pointSize: 12
        }
    }

    Rectangle {
        id: hobbieRect
        anchors.top: characterRect.bottom
        anchors.topMargin: 2
        anchors.left: parent.left
        anchors.right: parent.right
        height: parent.height/9
        color: "#515151"
        border.width: 1
        border.color: "#202020"
        radius: 5

        Rectangle{
            id: r5
            anchors.left: parent.left
            anchors.leftMargin: 3
            anchors.top: parent.top
            anchors.topMargin: 3
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 3
            width: r5.height
            radius: r5.height/2
            border.color: "#202020"
            border.width: 1
            color: "#909090"
            Image {
                id: i5
                source: "images/hobbiesIcon.png"
                anchors.fill: parent
                anchors.margins: 7
            }
        }
        Text {
            id: t5
            text: window.cardText[4]
            font.family: fontLoaderMaler.name
            anchors.left: r5.right
            anchors.leftMargin: 8
            anchors.right: parent.right
            anchors.rightMargin: 6
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            font.letterSpacing: 0.2
            color: "#FFCC33"
            wrapMode: Text.WordWrap
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignLeft
            font.pointSize: 12
        }
    }

    Rectangle {
        id: phobiaRect
        anchors.top: hobbieRect.bottom
        anchors.topMargin: 2
        anchors.left: parent.left
        anchors.right: parent.right
        height: parent.height/9
        color: "#515151"
        border.width: 1
        border.color: "#202020"
        radius: 5

        Rectangle{
            id: r6
            anchors.left: parent.left
            anchors.leftMargin: 3
            anchors.top: parent.top
            anchors.topMargin: 3
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 3
            width: r6.height
            radius: r6.height/2
            border.color: "#202020"
            border.width: 1
            color: "#909090"
            Image {
                id: i6
                source: "images/phobiaIcon.png"
                anchors.fill: parent
                anchors.margins: 7
            }
        }
        Text {
            id: t6
            text: window.cardText[5]
            font.family: fontLoaderMaler.name
            anchors.left: r6.right
            anchors.leftMargin: 8
            anchors.right: parent.right
            anchors.rightMargin: 6
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            font.letterSpacing: 0.2
            color: "#FFCC33"
            wrapMode: Text.WrapAnywhere
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignLeft
            font.pointSize: 12
        }
    }

    Rectangle {
        id: bagRect
        anchors.top: phobiaRect.bottom
        anchors.topMargin: 2
        anchors.left: parent.left
        anchors.right: parent.right
        height: parent.height/9
        color: "#515151"
        border.width: 1
        border.color: "#202020"
        radius: 5

        Rectangle{
            id: r7
            anchors.left: parent.left
            anchors.leftMargin: 3
            anchors.top: parent.top
            anchors.topMargin: 3
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 3
            width: r7.height
            radius: r7.height/2
            border.color: "#202020"
            border.width: 1
            color: "#909090"
            Image {
                id: i7
                source: "images/bagIcon.png"
                anchors.fill: parent
                anchors.margins: 7
            }
        }
        Text {
            id: t7
            text: window.cardText[6]
            font.family: fontLoaderMaler.name
            anchors.left: r7.right
            anchors.leftMargin: 8
            anchors.right: parent.right
            anchors.rightMargin: 6
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            font.letterSpacing: 0.2
            color: "#FFCC33"
            wrapMode: Text.WrapAnywhere
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignLeft
            font.pointSize: 12
        }
    }

    Rectangle {
        id: specialRect
        anchors.top: bagRect.bottom
        anchors.topMargin: 2
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 4
        color: "#515151"
        border.width: 1
        border.color: "#202020"
        radius: 5

        Rectangle{
            id: r8
            anchors.left: parent.left
            anchors.leftMargin: 3
            anchors.top: parent.top
            anchors.topMargin: 3
            height: 45
            width: r8.height
            radius: r8.height/2
            border.color: "#202020"
            border.width: 1
            color: "#909090"
            Image {
                id: i8
                source: "images/specialsIcon.png"
                anchors.fill: parent
                anchors.topMargin: 4
                anchors.bottomMargin: 7
                anchors.leftMargin: 7
                anchors.rightMargin: 7
            }
        }
        Text {
            id: t8
            text: window.cardText[7]
            font.family: fontLoaderMaler.name
            anchors.left: r8.right
            anchors.leftMargin: 8
            anchors.right: parent.right
            anchors.rightMargin: 6
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            font.letterSpacing: 0.2
            color: "#FFCC33"
            wrapMode: Text.WordWrap
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignLeft
            font.pointSize: 8
        }
    }

    Keys.onPressed: {
        if (event.key == Qt.Key_Escape) {
            settingsButtonVisibility = true;
            soundButtonVisibility = true;
            stackView.pop(StackView.Immediate);
            event.accepted = true;
        }
        if (event.key == Qt.Key_Back)
        {
            settingsButtonVisibility = true;
            soundButtonVisibility = true;
            stackView.pop(StackView.Immediate);
            event.accepted = true;
        }
    }
}
