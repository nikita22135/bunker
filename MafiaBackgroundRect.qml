import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.12

Rectangle{
    id: mafiaBackgroundRect
    Image {
        id: mafiaBackgroundImage
        anchors.fill: parent
        source: "images/mafiaBackground.png"
    }
}
