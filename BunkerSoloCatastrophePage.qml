import QtQml.Models 2.2
import QtQml 2.2
import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.12

SomePage {
    property int initialY: catastropheText.y
    Rectangle {
        id: bunkerSoloCatastropheBackground
        color: "#515151"
        anchors.fill: parent
    }
    Text {
        id: catastropheText
        text: window.catastropheText
        anchors.right: parent.right
        anchors.rightMargin: 6
        anchors.left: parent.left
        anchors.leftMargin: 6
        y: 2
        font.letterSpacing: 0.05
        color: "#FFFFFF"
        wrapMode: Text.WordWrap
        font.pointSize: 12
    }

    Timer {
        interval: 10;
        running: true;
        repeat: true
        onTriggered: bunkerSoloCatastrophePage.initialY = catastropheTextMouseArea.mouseY;
    }
    MouseArea{
        id: catastropheTextMouseArea
        anchors.fill: parent
        hoverEnabled: true

        onPositionChanged: {
            if ((catastropheTextMouseArea.mouseY > bunkerSoloCatastrophePage.initialY)&&(catastropheText.y<2)){
                catastropheText.y += 7;
            }
            else if ((catastropheTextMouseArea.mouseY<bunkerSoloCatastrophePage.initialY)&&(catastropheText.y>-500))
            {
                catastropheText.y -= 7;
            }
        }
    }

    Keys.onPressed: {
        if (event.key == Qt.Key_Escape) {
            settingsButtonVisibility = true;
            soundButtonVisibility = true;
            stackView.pop(StackView.Immediate);
            event.accepted = true;
        }
        if (event.key == Qt.Key_Back)
        {
            settingsButtonVisibility = true;
            soundButtonVisibility = true;
            stackView.pop(StackView.Immediate);
            event.accepted = true;
        }
    }
}
