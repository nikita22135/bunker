import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.12

Rectangle{
    id: bunkerNetworkButton
    color: bunkerNetworkButtonMouseArea.containsPress? Qt.darker(mainBunkerPage.buttonColor, 1.1) : mainBunkerPage.buttonColor
    radius: 15
    border.width: 2
    border.color: mainBunkerPage.buttonBorderColor
    signal bunkerNetworkButtonReleased();
    Text {
        id: bunkerNetworkButtonText
        FontLoader{
            id: fontLoader
            source: "fonts/Maler.ttf"
        }
        font.family: fontLoader.name
        
        text: qsTr("Игра по сети")
        anchors.centerIn: parent
        font.letterSpacing: 0.3
        font.pointSize: 14
    }
    MouseArea{
        id: bunkerNetworkButtonMouseArea
        anchors.fill: parent
        hoverEnabled: true
        onReleased: {
            bunkerNetworkButton.bunkerNetworkButtonReleased();
        }
    }
}
