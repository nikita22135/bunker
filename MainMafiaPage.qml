import QtQml.Models 2.2
import QtQml 2.2
import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.12
import QtGraphicalEffects 1.0

SomePage {
    id: root
    signal buttonMafiaBackReleased();
    signal buttonMafiaPlayReleased();
    signal buttonMafiaRulesReleased();
    MafiaBackgroundRect {
        id: mafiaBackgroundRect
        anchors.fill: parent
    }
    ButtonPlayMafia {
        id: buttonPlayMafia
        anchors.centerIn: parent
        width: parent.width/1.4
        height: parent.height/10
        anchors.verticalCenterOffset: -parent.height/8
        onButtonPlayMafiaReleased: {
            root.buttonMafiaPlayReleased();
        }
    }
    ButtonMafiaRules {
        id: buttonMafiaRules
        anchors.top: buttonPlayMafia.bottom
        anchors.left: buttonPlayMafia.left
        anchors.right: buttonPlayMafia.right
        height: parent.height/10
        anchors.topMargin: mainBunkerPage.buttonMargins
        onButtonMafiaRulesReleased: {
            root.buttonMafiaRulesReleased();
        }
    }
    ButtonMafiaBack {
        id: buttonMafiaBack
        anchors.top: buttonMafiaRules.bottom
        anchors.left: buttonMafiaRules.left
        anchors.right: buttonMafiaRules.right
        height: parent.height/10
        anchors.topMargin: mainBunkerPage.buttonMargins
        onButtonMafiaBackReleased: {
            root.buttonMafiaBackReleased();
        }
    }
    onButtonMafiaBackReleased: {
        settingsButton.isBunkerStyle = true;
        soundButton.isBunkerStyle = true;
        stackView.pop(StackView.Immediate);
    }
    onButtonMafiaPlayReleased: {
        stackView.push(playMafiaPage, StackView.Immediate);
    }
    onButtonMafiaRulesReleased: {
        stackView.push(mafiaRulesPage, StackView.Immediate);
        settingsButtonVisibility = false;
        soundButtonVisibility = false;
    }
}
