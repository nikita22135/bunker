import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.12

Rectangle{
    id: soundButton
    signal soundButtonReleased();
    property bool isMuted: settings.getMuted()
    property bool isBunkerStyle: true
    
    color: Qt.rgba(0,0,0,0)
    Image {
        id: soundIcon
        anchors.fill: parent
        source: isMuted ? (isBunkerStyle ? "images/soundOff.png" : "images/soundOffMafia.png") : (isBunkerStyle ? "images/soundOn.png" : "images/soundOnMafia.png")
    }
    MouseArea{
        id: soundButtonMouseArea
        anchors.fill: parent
        hoverEnabled: true
        onReleased: {
            isMuted = !isMuted;
            settings.setMuted(isMuted);
        }
    }
}
