import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.12

Rectangle{
    id: backButton
    signal backButtonCliked();
    color: Qt.rgba(0,0,0,0)
    
    Image {
        id: backImage
        anchors.fill: parent
        source: "images/backIcon.png"
    }
    
    MouseArea{
        id: backButtonMouseArea
        anchors.fill: parent
        hoverEnabled: true
        onClicked: {
            bunkerRulesPage.backButtonClicked();
        }
    }
}
