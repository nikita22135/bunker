import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.12

Rectangle{
    id: buttonMafia
    signal buttonMafiaReleased();
    color: mafiaButtonMouseArea.containsPress? "#161616" : mainPage.buttonMafiaColor
    border.width: 2
    border.color: "#333333"
    radius: 15
    Image {
        id: mafiaIcon
        property int iconMargin: 3
        width: parent.height - 3
        height: parent.height - 3
        anchors.left: parent.left
        anchors.leftMargin: iconMargin
        anchors.verticalCenter: parent.verticalCenter
        source: "images/mafiaIcon.png"
    }
    Text {
        id: mafiaButtonText
        color: "#CC0000"
//        FontLoader{
//            id: fontLoader
//            source: "fonts/Lombardina One.ttf"
//        }
        font.pointSize: 18
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.horizontalCenterOffset: 13
        
        text: qsTr("Мафия")
        FontLoader{
            id: fontLoader
            source: "fonts/Maler.ttf"
        }
//        font.family: fontLoader.name
        font.family: fontLoader.name
    }
    MouseArea{
        id: mafiaButtonMouseArea
        anchors.fill: parent
        hoverEnabled: true
        onReleased: {
            buttonMafia.buttonMafiaReleased();
        }
    }
}
