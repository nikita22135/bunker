#include "bunkerclient.h"
#include <QDebug>
#include <QtNetwork>
BunkerClient::BunkerClient(QObject *parent) : QObject(parent)
{
    _socket = new QTcpSocket(this);
    //_socket->setSocketDescriptor(desc);
    connect(_socket, &QTcpSocket::connected,this, &BunkerClient::onConnected);
    connect(_socket, &QTcpSocket::readyRead, this, &BunkerClient::readData);
    _nicknamesList.clear();
}

void BunkerClient::connectToServer()
{
    _socket->connectToHost(QHostAddress("127.0.0.1"), 1234);
}

void BunkerClient::writeData(QString data)
{
    if (data.contains("connectToBunkerHostWithId"))
    {
        qWarning()<<data;
    }
    _socket->write(QByteArray(data.toUtf8()));
}

int BunkerClient::getId()
{
    return id;
}

void BunkerClient::sendCatastropheParametersSlot(int catastropheId, QList<int> itemsList, int electricity, int peopleCount)
{
    QString str = "";
    str.append("catastropheId:"+QString::number(catastropheId)+"\r\n");
    foreach (int item, itemsList)
    {
        str.append("itemId:"+QString::number(item)+"\r\n");
    }
    str.append("electricityId:"+QString::number(electricity)+"\r\n");
    str.append("peopleCount:"+QString::number(peopleCount)+"\r\n");
    _socket->write(QByteArray(str.toUtf8()));
}

void BunkerClient::onConnected()
{
    qDebug()<<"connected";
}

void BunkerClient::readData()
{
    QStringList bunkerHosts;
    QList<int> bunkerHostsIds;
    QList<int> bunkerPlayersCount;
    QList<bool> bunkerIsPass;
    bunkerPlayersCount.clear();
    QString data = _socket->readAll();
    if (data.contains("yourId:"))
    {
        id = data.mid(data.indexOf("yourId:")+7,data.indexOf('\r')-data.indexOf("yourId:")-7).toInt();
        data.remove(data.indexOf("yourId:"),data.indexOf('\n')+1);
        emit idCreated();
    }
    if (data.contains("connectionToBunkerLobbyApproved"))
    {
        data.remove(data.indexOf("connectionToBunkerLobbyApproved"),33);
        qWarning()<<"yeah";
        emit connectionToLobbyApproved();
    }
    if (data.contains("hostLobbyOnServerBunkerApproved"))
    {
        emit hostBunkerApproved();
        data.remove(data.indexOf("hostLobbyOnServerBunkerApproved"),32);
    }
    if (data.contains("clearNicknamesList"))
    {
        _nicknamesList.clear();
        data.remove(data.indexOf("clearNicknamesList"), 18);
    }
    while (data.contains("nickname:")) //это всегда должно идти последним
    {
        data.remove(data.indexOf("nickname:"), 9);
        _nicknamesList.append(data.mid(0,data.indexOf("nickname:")));
        if (data.contains("nickname:"))
            data.remove(0,data.indexOf("nickname:"));
        else
            data.remove(0,data.length());
        emit nicknamesListUpdated(_nicknamesList);
    }
    while (data.contains("BunkerLobbyHosts:"))
    {
        data.remove(data.indexOf("BunkerLobbyHosts:"),17);
        if (data.contains("BunkerLobbyHosts:"))
            bunkerHosts.append(data.mid(0,data.indexOf("BunkerLobbyHosts:")));
        else
            bunkerHosts.append(data.mid(0,data.indexOf("BunkerLobbyIds:")));
        if (data.contains("BunkerLobbyHosts:"))
            data.remove(0,data.indexOf("BunkerLobbyHosts:"));
        else
        {
            if (data.contains("BunkerLobbyIds:"))
                data.remove(0,data.indexOf("BunkerLobbyIds:"));
            else
                data.remove(0,data.length());
        }
        emit bunkerLobbyHostsListUpdated(bunkerHosts);
    }
    while (data.contains("BunkerLobbyIds:"))
    {
        data.remove(data.indexOf("BunkerLobbyIds:"),15);
        if (data.contains("BunkerLobbyIds:"))
            bunkerHostsIds.append(data.mid(0,data.indexOf("BunkerLobbyIds:")).toInt());
        else
            bunkerHostsIds.append(data.mid(0,data.indexOf("BunkerPlayersCount:")).toInt());
        if (data.contains("BunkerLobbyIds:"))
            data.remove(0,data.indexOf("BunkerLobbyIds:"));
        else
        {
            if (data.contains("BunkerPlayersCount:"))
                data.remove(0,data.indexOf("BunkerPlayersCount:"));
            else
                data.remove(0,data.length());
        }
        emit bunkerLobbyHostsIdsListUpdated(bunkerHostsIds);
    }
    while (data.contains("BunkerPlayersCount:"))
    {
        data.remove(data.indexOf("BunkerPlayersCount:"),19);
        if (data.contains("BunkerPlayersCount:"))
            bunkerPlayersCount.append(data.mid(0, data.indexOf("BunkerPlayersCount:")).toInt());
        else
            bunkerPlayersCount.append(data.mid(0, data.indexOf("BunkerIsPass:")).toInt());
        if (data.contains("BunkerPlayersCount:"))
            data.remove(0,data.indexOf("BunkerPlayersCount:"));
        else
        {
            if (data.contains("BunkerIsPass:"))
                data.remove(0,data.indexOf("BunkerIsPass:"));
            else
                data.remove(0,data.length());
        }
        emit bunkerPlayersCountUpdated(bunkerPlayersCount);
    }
    while (data.contains("BunkerIsPass:"))
    {
        data.remove(data.indexOf("BunkerIsPass:"),13);
        bunkerIsPass.append(data.mid(0, data.indexOf("BunkerIsPass:")).toInt());
        if (data.contains("BunkerIsPass:"))
            data.remove(0,data.indexOf("BunkerIsPass:"));
        else
            data.remove(0,data.length());
        emit bunkerIsPassUpdated(bunkerIsPass);
    }
    if (data.contains("cantStartGameReason:tooFewPlayers"))
    {
        data.remove(data.indexOf("cantStartGameReason:tooFewPlayers"),35);
        emit gameIsNotStarted();
    }
    if (data.contains("startBunkerGame"))
    {
        data.remove(data.indexOf("startBunkerGame"),17);
        emit startBunkerGame();
    }
    if (data.contains("catastropheId:"))
    {
        if (data.contains("catastropheId:"))
        {
            QList<int> itemsId;
            itemsId.clear();
            int catastropeId = data.mid(data.indexOf("catastropheId:")+14,data.indexOf('\r')-(data.indexOf("catastropheId:")+14)).toInt();
            data.remove(data.indexOf("catastropheId:")+14,
                        data.indexOf('\n')-(data.indexOf("catastropheId:")+14)+1);
            data.remove(data.indexOf("catastropheId:"),14);
            while (data.contains("itemId:"))
            {
                itemsId.append(data.mid(data.indexOf("itemId:")+7,data.indexOf('\r')-(data.indexOf("itemId:")+7)).toInt());
                data.remove(data.indexOf("itemId:")+7,
                            data.indexOf('\n')-(data.indexOf("itemId:")+7)+1);
                data.remove(data.indexOf("itemId:"),7);
            }
            int electricityId = data.mid(data.indexOf("electricityId:")+14,data.indexOf('\r')-(data.indexOf("electricityId:")+14)).toInt();
            data.remove(data.indexOf("electricityId:")+14,
                        data.indexOf('\n')-(data.indexOf("electricityId:")+14)+1);
            data.remove(data.indexOf("electricityId:"),14);
            int peopleCount = data.mid(data.indexOf("peopleCount:")+12,data.indexOf('\r')-(data.indexOf("peopleCount:")+12)).toInt();
            data.remove(data.indexOf("peopleCount:")+12,
                        data.indexOf('\n')-(data.indexOf("peopleCount:")+12)+1);
            data.remove(data.indexOf("peopleCount:"),12);
            emit sendCatastropheParameters(catastropeId, itemsId, electricityId, peopleCount);
        }
    }
    if (data.contains("notEnoughSlotsToConnect"))
    {
        data.remove(data.indexOf("notEnoughSlotsToConnect"),25);
        emit notEnoughSpace();
    }
    if (data.contains("passwordIsNotMatch"))
    {
        data.remove(data.indexOf("passwordIsNotMatch"),20);
        emit passIsNotMatch();
    }
    if (data.contains("biologyOfPlayerWithId:"))
    {
        QString biology;
        biology.clear();
        biology = data.mid(data.indexOf("biologyOfPlayerWithId:")+22,
                           data.indexOf('\r')-(data.indexOf("biologyOfPlayerWithId:")+22));
        data.remove(data.indexOf("biologyOfPlayerWithId:")+22,
                    data.indexOf('\n')-(data.indexOf("biologyOfPlayerWithId:")+22)+1);
        data.remove(data.indexOf("biologyOfPlayerWithId:"),22);
        int number = data.mid(data.indexOf("myId:")+5,
                              data.indexOf('\r')-(data.indexOf("myId:")+5)).toInt();
        data.remove(data.indexOf("myId:")+5,
                    data.indexOf('\n')-(data.indexOf("myId:")+5)+1);
        data.remove(data.indexOf("myId:"),5);
        emit biologyComes(biology, number);
    }
    if (data.contains("professionOfPlayerWithId:"))
    {
        QString profession;
        profession.clear();
        profession = data.mid(data.indexOf("professionOfPlayerWithId:")+25,
                           data.indexOf('\r')-(data.indexOf("professionOfPlayerWithId:")+25));
        data.remove(data.indexOf("professionOfPlayerWithId:")+25,
                    data.indexOf('\n')-(data.indexOf("professionOfPlayerWithId:")+25)+1);
        data.remove(data.indexOf("professionOfPlayerWithId:"),25);
        int number = data.mid(data.indexOf("myId:")+5,
                              data.indexOf('\r')-(data.indexOf("myId:")+5)).toInt();
        data.remove(data.indexOf("myId:")+5,
                    data.indexOf('\n')-(data.indexOf("myId:")+5)+1);
        data.remove(data.indexOf("myId:"),5);
        emit professionComes(profession, number);
    }
    if (data.contains("healthOfPlayerWithId:"))
    {
        QString health;
        health.clear();
        health = data.mid(data.indexOf("healthOfPlayerWithId:")+21,
                           data.indexOf('\r')-(data.indexOf("healthOfPlayerWithId:")+21));
        data.remove(data.indexOf("healthOfPlayerWithId:")+21,
                    data.indexOf('\n')-(data.indexOf("healthOfPlayerWithId:")+21)+1);
        data.remove(data.indexOf("healthOfPlayerWithId:"),21);
        int number = data.mid(data.indexOf("myId:")+5,
                              data.indexOf('\r')-(data.indexOf("myId:")+5)).toInt();
        data.remove(data.indexOf("myId:")+5,
                    data.indexOf('\n')-(data.indexOf("myId:")+5)+1);
        data.remove(data.indexOf("myId:"),5);
        emit healthComes(health, number);
    }
    if (data.contains("characterOfPlayerWithId:"))
    {
        QString character;
        character.clear();
        character = data.mid(data.indexOf("characterOfPlayerWithId:")+24,
                           data.indexOf('\r')-(data.indexOf("characterOfPlayerWithId:")+24));
        data.remove(data.indexOf("characterOfPlayerWithId:")+24,
                    data.indexOf('\n')-(data.indexOf("characterOfPlayerWithId:")+24)+1);
        data.remove(data.indexOf("characterOfPlayerWithId:"),24);
        int number = data.mid(data.indexOf("myId:")+5,
                              data.indexOf('\r')-(data.indexOf("myId:")+5)).toInt();
        data.remove(data.indexOf("myId:")+5,
                    data.indexOf('\n')-(data.indexOf("myId:")+5)+1);
        data.remove(data.indexOf("myId:"),5);
        emit characterComes(character, number);
    }
    if (data.contains("hobbieOfPlayerWithId:"))
    {
        QString hobbie;
        hobbie.clear();
        hobbie = data.mid(data.indexOf("hobbieOfPlayerWithId:")+21,
                           data.indexOf('\r')-(data.indexOf("hobbieOfPlayerWithId:")+21));
        data.remove(data.indexOf("hobbieOfPlayerWithId:")+21,
                    data.indexOf('\n')-(data.indexOf("hobbieOfPlayerWithId:")+21)+1);
        data.remove(data.indexOf("hobbieOfPlayerWithId:"),21);
        int number = data.mid(data.indexOf("myId:")+5,
                              data.indexOf('\r')-(data.indexOf("myId:")+5)).toInt();
        data.remove(data.indexOf("myId:")+5,
                    data.indexOf('\n')-(data.indexOf("myId:")+5)+1);
        data.remove(data.indexOf("myId:"),5);
        emit hobbieComes(hobbie, number);
    }
    if (data.contains("phobiaOfPlayerWithId:"))
    {
        QString phobia;
        phobia.clear();
        phobia = data.mid(data.indexOf("phobiaOfPlayerWithId:")+21,
                           data.indexOf('\r')-(data.indexOf("phobiaOfPlayerWithId:")+21));
        data.remove(data.indexOf("phobiaOfPlayerWithId:")+21,
                    data.indexOf('\n')-(data.indexOf("phobiaOfPlayerWithId:")+21)+1);
        data.remove(data.indexOf("phobiaOfPlayerWithId:"),21);
        int number = data.mid(data.indexOf("myId:")+5,
                              data.indexOf('\r')-(data.indexOf("myId:")+5)).toInt();
        data.remove(data.indexOf("myId:")+5,
                    data.indexOf('\n')-(data.indexOf("myId:")+5)+1);
        data.remove(data.indexOf("myId:"),5);
        emit phobiaComes(phobia, number);
    }
    if (data.contains("bagOfPlayerWithId:"))
    {
        QString bag;
        bag.clear();
        bag = data.mid(data.indexOf("bagOfPlayerWithId:")+18,
                           data.indexOf('\r')-(data.indexOf("bagOfPlayerWithId:")+18));
        data.remove(data.indexOf("bagOfPlayerWithId:")+18,
                    data.indexOf('\n')-(data.indexOf("bagOfPlayerWithId:")+18)+1);
        data.remove(data.indexOf("bagOfPlayerWithId:"),18);
        int number = data.mid(data.indexOf("myId:")+5,
                              data.indexOf('\r')-(data.indexOf("myId:")+5)).toInt();
        data.remove(data.indexOf("myId:")+5,
                    data.indexOf('\n')-(data.indexOf("myId:")+5)+1);
        data.remove(data.indexOf("myId:"),5);
        emit bagComes(bag, number);
    }
    if (data.contains("special1OfPlayerWithId:"))
    {
        QString special1;
        special1.clear();
        special1 = data.mid(data.indexOf("special1OfPlayerWithId:")+23,
                           data.indexOf('\r')-(data.indexOf("special1OfPlayerWithId:")+23));
        data.remove(data.indexOf("special1OfPlayerWithId:")+23,
                    data.indexOf('\n')-(data.indexOf("special1OfPlayerWithId:")+23)+1);
        data.remove(data.indexOf("special1OfPlayerWithId:"),23);
        int number = data.mid(data.indexOf("myId:")+5,
                              data.indexOf('\r')-(data.indexOf("myId:")+5)).toInt();
        data.remove(data.indexOf("myId:")+5,
                    data.indexOf('\n')-(data.indexOf("myId:")+5)+1);
        data.remove(data.indexOf("myId:"),5);
        emit special1Comes(special1, number);
    }
    if (data.contains("changeHealthToRandom"))
    {
        data.remove(data.indexOf("changeHealthToRandom"),22);
        int playerNum = data.mid(data.indexOf("playerNum:")+10,
                                 data.indexOf('\r')-(data.indexOf("playerNum:")+10)).toInt();
        data.remove(data.indexOf("playerNum:")+10,
                    data.indexOf('\n')-(data.indexOf("playerNum:")+10)+1);
        data.remove(data.indexOf("playerNum:"),10);
        emit changeHealthToRandom(playerNum);
    }
}
