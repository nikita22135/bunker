import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.12

Rectangle{
    id: buttonRules
    color: rulesButtonMouseArea.containsPress? Qt.darker(mainBunkerPage.buttonColor, 1.1) : mainBunkerPage.buttonColor
    radius: 15
    border.width: 2
    border.color: mainBunkerPage.buttonBorderColor
    signal rulesButtonReleased();
    Text {
        id: rulesText
        FontLoader{
            id: fontLoader
            source: "fonts/Maler.ttf"
        }
        font.family: fontLoader.name
        
        text: qsTr("Правила")
        anchors.centerIn: parent
        font.pointSize: 14
    }
    MouseArea{
        id: rulesButtonMouseArea
        anchors.fill: parent
        hoverEnabled: true
        onReleased: {
            buttonRules.rulesButtonReleased();
        }
    }
}
