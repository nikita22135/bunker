import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.12

Rectangle{
    id: bunkerOnePhoneButton
    signal bunkerOnePhoneButtonReleased();
    color: bunkerOnePhoneMouseArea.containsPress? Qt.darker(mainBunkerPage.buttonColor, 1.1) : mainBunkerPage.buttonColor
    radius: 15
    border.width: 2
    border.color: mainBunkerPage.buttonBorderColor
    signal playButtonReleased();
    Text {
        id: bunkerOnePhoneText
        FontLoader{
            id: fontLoader
            source: "fonts/Maler.ttf"
        }
        font.family: fontLoader.name
        
        text: qsTr("Игра оффлайн")
        anchors.centerIn: parent
        font.pointSize: 14
    }
    MouseArea{
        id: bunkerOnePhoneMouseArea
        anchors.fill: parent
        hoverEnabled: true
        onReleased: {
            bunkerOnePhoneButton.bunkerOnePhoneButtonReleased();
        }
    }
}
