import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.12

Rectangle{
    id: buttonBack
    color: backButtonMouseArea.containsPress? Qt.darker(mainBunkerPage.buttonColor, 1.1) : mainBunkerPage.buttonColor
    radius: 15
    border.width: 2
    border.color: mainBunkerPage.buttonBorderColor
    property alias buttonText: backText.text
    signal backButtonReleased();
    Text {
        id: backText
        FontLoader{
            id: fontLoader
            source: "fonts/Maler.ttf"
        }
        font.family: fontLoader.name
        
        anchors.centerIn: parent
        font.pointSize: 14
    }
    MouseArea{
        id: backButtonMouseArea
        anchors.fill: parent
        hoverEnabled: true
        onReleased: {
            buttonBack.backButtonReleased();
        }
    }
}
