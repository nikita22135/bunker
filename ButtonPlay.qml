import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.12

Rectangle{
    id: buttonPlay
    color: playButtonMouseArea.containsPress? Qt.darker(mainBunkerPage.buttonColor, 1.1) : mainBunkerPage.buttonColor
    radius: 15
    border.width: 2
    border.color: mainBunkerPage.buttonBorderColor
    property alias buttonText: playText.text
    signal playButtonReleased();
    Text {
        id: playText
        FontLoader{
            id: fontLoader
            source: "fonts/Maler.ttf"
        }
        font.family: fontLoader.name
        text: qsTr("Играть")
        anchors.centerIn: parent
        font.pointSize: 14
    }
    MouseArea{
        id: playButtonMouseArea
        anchors.fill: parent
        hoverEnabled: true
        onReleased: {
            buttonPlay.playButtonReleased();
        }
    }
}
