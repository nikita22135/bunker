#include "bunkercardgenerator.h"
#include <QFile>
#include <QDebug>
#include <random>
#include <ctime>
#include <unistd.h>
BunkerCardGenerator::BunkerCardGenerator(QObject *parent) : QObject(parent)
{
    _bagFile = new QFile(":/bunkerCharacters/bag.txt");
    _healthFile = new QFile(":/bunkerCharacters/health.txt");
    _hobbiesFile = new QFile(":/bunkerCharacters/hobbies.txt");
    _phobiasFile = new QFile(":/bunkerCharacters/phobias.txt");
    _professionFile = new QFile(":/bunkerCharacters/profession.txt");
    _specialsFile = new QFile(":/bunkerCharacters/special.txt");
    _maleCharacterFile = new QFile(":/bunkerCharacters/male_character.txt");
    _femaleCharacterFile = new QFile(":/bunkerCharacters/female_character.txt");
    _catastropheFile = new QFile(":/bunkerCatastrophes/catastrophes.txt");
    _bunkerItemsFile = new QFile(":/bunkerCatastrophes/bunkerItems.txt");
}

QStringList BunkerCardGenerator::generateCard()
{
    std::srand(std::time(nullptr)+_id);
    QStringList card;
    card.clear();
    _bagFile->open(QIODevice::ReadOnly);
    _healthFile->open(QIODevice::ReadOnly);
    _hobbiesFile->open(QIODevice::ReadOnly);
    _phobiasFile->open(QIODevice::ReadOnly);
    _professionFile->open(QIODevice::ReadOnly);
    _specialsFile->open(QIODevice::ReadOnly);
    _maleCharacterFile->open(QIODevice::ReadOnly);
    _femaleCharacterFile->open(QIODevice::ReadOnly);
    QStringList bag;
    QStringList health;
    QStringList hobbies;
    QStringList phobias;
    QStringList profession;
    QStringList specials;
    QStringList maleCharacter;
    QStringList femaleCharacter;
    bag.clear();
    health.clear();
    hobbies.clear();
    phobias.clear();
    profession.clear();
    specials.clear();
    maleCharacter.clear();
    femaleCharacter.clear();
    while (!_bagFile->atEnd())
    {
        bag.append(_bagFile->readLine());
        bag.last().chop(2);
    }

    while (!_healthFile->atEnd())
    {
        health.append(_healthFile->readLine());
        health.last().chop(2);
    }

    while (!_hobbiesFile->atEnd())
    {
        hobbies.append(_hobbiesFile->readLine());
        hobbies.last().chop(2);
    }

    while (!_phobiasFile->atEnd())
    {
        phobias.append(_phobiasFile->readLine());
        phobias.last().chop(2);
    }

    while (!_professionFile->atEnd())
    {
        profession.append(_professionFile->readLine());
        profession.last().chop(2);
    }

    while (!_specialsFile->atEnd())
    {
        specials.append(_specialsFile->readLine());
        specials.last().chop(2);
    }

    while (!_maleCharacterFile->atEnd())
    {
        maleCharacter.append(_maleCharacterFile->readLine());
        maleCharacter.last().chop(2);
    }

    while (!_femaleCharacterFile->atEnd())
    {
        femaleCharacter.append(_femaleCharacterFile->readLine());
        femaleCharacter.last().chop(2);
    }

    int sum = 0;

    do
    {
        sum = 0;
        int randomSex = abs(std::rand() % 2);
        int randomAge = abs(std::rand() % 82) + 18;
        int randomOrientation = abs(std::rand() % 4);

        if (randomAge<=40)
        {
            sum += 10;
        }
        else if (randomAge<=60)
        {
            sum += 5;
        }
        else
        {
            sum += 0;
        }

        if ((randomOrientation == 0)||(randomOrientation == 1))
        {
            sum += 10;
        }
        else if (randomOrientation == 2)
        {
            sum += 3;
        }
        else
        {
            sum += 0;
        }

        QString biology("");

        if (randomSex == 0)
            biology.append("Мужчина ");
        else
            biology.append("Женщина ");

        biology.append(QString::number(randomAge));
        if (randomAge/10 == 1)
            biology.append(" лет ");
        else
        {
            if (randomAge%10 == 1)
                biology.append(" год ");
            else if ((randomAge%10 == 2)||(randomAge%10 == 3)||(randomAge%10 == 4))
                biology.append(" года ");
            else
                biology.append(" лет ");
        }

        if (randomSex == 0)
        {
            switch (randomOrientation)
            {
            case 0:
                biology.append("гетеросексуален");
                break;
            case 1:
                biology.append("бисексуален");
                break;
            case 2:
                biology.append("гомосексуален");
                break;
            case 3:
                biology.append("чайлдфри");
                break;
            }
        }
        else if (randomSex == 1)
        {
            switch (randomOrientation)
            {
            case 0:
                biology.append("гетеросексуальна");
                break;
            case 1:
                biology.append("бисексуальна");
                break;
            case 2:
                biology.append("гомосексуальна");
                break;
            case 3:
                biology.append("чайлдфри");
                break;
            }
        }

        card.append(biology);
        card.append(simplifyLine(profession, sum));
        card.append(simplifyLine(health, sum));
        if (randomSex == 0)
        {
            card.append(simplifyLine(maleCharacter, sum));
        }
        else if (randomSex == 1)
        {
            card.append(simplifyLine(femaleCharacter, sum));
        }
        card.append(simplifyLine(hobbies, sum));
        card.append(simplifyLine(phobias, sum));
        card.append(simplifyLine(bag, sum));
        card.append(simplifyLine(specials, sum));
        qWarning()<<sum;
        usleep(100);
        if ((sum >= 80) || (sum <= 70))
            card.clear();
    }while ((sum >= 80)||(sum <= 70)); //иногда тут бывают проблемы

    _bagFile->close();
    _healthFile->close();
    _hobbiesFile->close();
    _phobiasFile->close();
    _professionFile->close();
    _specialsFile->close();
    _maleCharacterFile->close();
    _femaleCharacterFile->close();

    qWarning()<<card;
    emit cardGenerated(card);
    return card;
}

QString BunkerCardGenerator::generateCatastrophe()
{
    _catastropheFile->open(QIODevice::ReadOnly);
    _bunkerItemsFile->open(QIODevice::ReadOnly);
    _itemsList.clear();
    QStringList catastropheList;
    QStringList itemsList;
    catastropheList.clear();
    itemsList.clear();
    while(!_catastropheFile->atEnd())
    {
        catastropheList.append(_catastropheFile->readLine());
        catastropheList.last().chop(2);
    }
    while (!_bunkerItemsFile->atEnd())
    {
        itemsList.append(_bunkerItemsFile->readLine());
        itemsList.last().chop(2);
    }
    int randomLine = abs(std::rand()%(catastropheList.length()));
    _catastropheFile->close();
    QString catastropheLine = catastropheList.at(randomLine);
    int randomPeopleCount = 0;
    int randomElectricity = abs(std::rand()%3);
    int randomItemsCount = abs(std::rand()%4);
    _electricity = randomElectricity;
    _catastropheId = randomLine;
    switch (randomLine)
    {
    case 0:
        randomPeopleCount = abs((std::rand()%2000000)+1000000);
        catastropheLine.append("\nПриблизительное количество выживших: "+QString::number(randomPeopleCount)+"\n");
        break;
    case 1:
        randomPeopleCount = abs((std::rand()%3000000)+2000000);
        catastropheLine.append("\nПриблизительное количество выживших: "+QString::number(randomPeopleCount)+"\n");
        break;
    case 2:
        randomPeopleCount = abs((std::rand()%30000)+8000);
        catastropheLine.append("\nПриблизительное количество выживших: "+QString::number(randomPeopleCount)+"\n");
        break;
    case 3:
        randomPeopleCount = abs((std::rand()%7000000)+2000000);
        catastropheLine.append("\nПриблизительное количество выживших: "+QString::number(randomPeopleCount)+"\n");
        break;
    }
    _peopleCount = randomPeopleCount;
    switch (randomElectricity) {
    case 0:
        catastropheLine.append("Электричество в бункере работает стабильно\n");
        break;
    case 1:
        catastropheLine.append("Имеются перебои в работе электричества\n");
        break;
    case 2:
        catastropheLine.append("Электричества нет. Работает только аварийное освещение\n");
        break;
    }
    catastropheLine.append("В бункере имеются:\nЗапас пресной воды и еды на все время пребывания\n");
    int randArr[randomItemsCount];
    for (int i = 0; i<randomItemsCount; i++)
    {
        int rand = abs(std::rand()%(itemsList.length()));
        randArr[i] = rand;
        bool isUniqe = false;
        while (!isUniqe)
        {
            isUniqe = true;
            for (int j =0; j<i;j++)
            {
                if (randArr[j] == rand)
                {
                    isUniqe = false;
                }
            }
            if (!isUniqe)
            {
                rand = abs(std::rand()%(itemsList.length()));
                _itemsList.append(rand);
            }
        }
        catastropheLine.append(itemsList.at(rand)+"\n");
    }
    _bunkerItemsFile->close();
    return catastropheLine;
}

void BunkerCardGenerator::setId(int id)
{
    _id = id;
}

void BunkerCardGenerator::generateCatastropheByParameters(int catastropheId, QList<int> itemsList, int electricity, int peopleCount)
{
    _catastropheFile->open(QIODevice::ReadOnly);
    _bunkerItemsFile->open(QIODevice::ReadOnly);
    _itemsList.clear();
    QStringList catastropheList;
    QStringList itemsLst;
    catastropheList.clear();
    itemsLst.clear();
    while(!_catastropheFile->atEnd())
    {
        catastropheList.append(_catastropheFile->readLine());
        catastropheList.last().chop(2);
    }
    while (!_bunkerItemsFile->atEnd())
    {
        itemsLst.append(_bunkerItemsFile->readLine());
        itemsLst.last().chop(2);
    }
    _catastropheFile->close();
    QString catasropheLine = catastropheList.at(catastropheId);
    catasropheLine.append("\nПриблизительное количество выживших: "+QString::number(peopleCount)+"\n");
    switch (electricity)
    {
    case 0:
        catasropheLine.append("Электричество в бункере работает стабильно\n");
        break;
    case 1:
        catasropheLine.append("Имеются перебои в работе электричества\n");
        break;
    case 2:
        catasropheLine.append("Электричества нет. Работает только аварийное освещение\n");
        break;
    }
    catasropheLine.append("В бункере имеются:\nЗапас пресной воды и еды на все время пребывания\n");
    foreach (auto item, itemsList)
    {
        catasropheLine.append(itemsLst.at(item)+"\n");
    }
    _bunkerItemsFile->close();
    emit catastropheByParametersGenerated(catasropheLine);
}

void BunkerCardGenerator::generateRandomHealth(int playerNum)
{
    _healthFile->open(QIODevice::ReadOnly);
    QStringList healthList;
    healthList.clear();
    while (!_healthFile->atEnd()) {
        healthList.append(_healthFile->readLine());
    }
    _healthFile->close();
    int sum = 0;
    QString health = simplifyLine(healthList, sum);
    emit newHealthGenerated(health, playerNum);
}

QString BunkerCardGenerator::sendCatastropheParameters()
{
    QString str = generateCatastrophe();
    emit sendCatastropheParametersSignal(_catastropheId, _itemsList, _electricity, _peopleCount);
    return str;
}

void BunkerCardGenerator::findWeight(QString &str)
{
    while (str.front() != '1' && str.front() != '2' && str.front() != '3' &&
           str.front() != '4' && str.front() != '5' && str.front() != '6' &&
           str.front() != '7' && str.front() != '8' && str.front() != '9' &&
           str.front() != '0')
    {
        str.remove(0,1);
    }
}

QString BunkerCardGenerator::simplifyLine(QStringList list, int &sum)
{
    int randomLine = abs(std::rand() % (list.length()-1));
    QString str = list.at(randomLine);
    QString strEnd = str.mid(str.length()-3);
    findWeight(strEnd);
    str.chop(strEnd.length()+1);
    sum += strEnd.toInt();
    return str;
}
