import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.12

Rectangle{
    id: buttonExit
    
    color: exitButtonMouseArea.containsPress? Qt.darker(mainPage.buttonExitColor, 1.1) : mainPage.buttonExitColor
    radius: 15
    border.width: 2
    border.color: mainBunkerPage.buttonBorderColor
    signal exitButtonReleased();

    Image {
        id: exitIcon
        property int iconMargin: 3
        width: parent.height - 3
        height: parent.height - 8
        anchors.left: parent.left
        anchors.leftMargin: iconMargin
        anchors.topMargin: 5
        anchors.verticalCenter: parent.verticalCenter
        source: "images/exitIcon.png"
    }

    Text {
        id: exitText
        FontLoader{
            id: fontLoader
            source: "fonts/Maler.ttf"
        }
        font.family: fontLoader.name
        
        text: qsTr("Выход")
        //color: "#CC0000"
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.horizontalCenterOffset: 10
        font.pointSize: 18
    }

    MouseArea{
        id: exitButtonMouseArea
        anchors.fill: parent
        hoverEnabled: true
        onReleased: {
            buttonExit.exitButtonReleased();
            Qt.quit();
        }
    }
}
