#ifndef SETTINGS_H
#define SETTINGS_H

#include <QObject>

class Settings : public QObject
{
    Q_OBJECT
public:
    explicit Settings(QObject* parent = nullptr);

    Q_INVOKABLE QString getNickname() const;
    Q_INVOKABLE void setNickname(const QString& value);
    Q_INVOKABLE bool getMuted() const;
    Q_INVOKABLE void setMuted(const bool& value);
    Q_INVOKABLE int getAvatarNumber() const;
    Q_INVOKABLE void setAvatarNumber(int value);

private:
    void muteSound();

    QString nickname;
    bool isMuted;
    int avatarNumber;
};

#endif // SETTINGS_H
