import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.12

Rectangle{
    id: buttonPlayMafia
    signal buttonPlayMafiaReleased();
    color: playMafiaButtonMouseArea.containsPress? "#161616" : mainPage.buttonMafiaColor
    border.width: 2
    border.color: "#333333"
    radius: 15
    Text {
        id: playMafiaButtonText
        color: "#CC0000"
        FontLoader{
            id: fontLoader
            source: "fonts/Maler.ttf"
        }
        font.pointSize: 14
        anchors.centerIn: parent
        text: qsTr("Играть")
        font.family: fontLoader.name
    }
    MouseArea{
        id: playMafiaButtonMouseArea
        anchors.fill: parent
        hoverEnabled: true
        onReleased: {
            buttonPlayMafia.buttonPlayMafiaReleased();
        }
    }
}
