import QtQml.Models 2.2
import QtQml 2.2
import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.12

SomePage {
    id: root
    backgroundColor: Qt.rgba(0,0,0,0.7)
    property int buttonMargins: 10
    property color buttonBunkerColor: "#FFCC33"
    property color buttonMafiaColor: "#000000"
    property color buttonExitColor: "#CC0000"
    signal buttonBunkerReleased();
    signal buttonMafiaReleased();

    Image {
        id: mainPageBackground
        anchors.fill: parent
        source: "images/mainBackground.jpg"
    }
    ButtonBunker {
        id: buttonBunker
        anchors.centerIn: parent
        width: parent.width/1.4
        height: parent.height/10
        anchors.verticalCenterOffset: -parent.height/8
        onButtonBunkerReleased: {
            root.buttonBunkerReleased();
        }
    }
    ButtonMafia {
        id: buttonMafia
        anchors.top: buttonBunker.bottom
        anchors.left: buttonBunker.left
        anchors.right: buttonBunker.right
        anchors.topMargin: mainBunkerPage.buttonMargins
        height: parent.height/10
        onButtonMafiaReleased: {
            root.buttonMafiaReleased();
        }
    }
    ButtonExit {
        id: buttonExit
        anchors.top: buttonMafia.bottom
        anchors.left: buttonMafia.left
        anchors.right: buttonMafia.right
        anchors.topMargin: mainBunkerPage.buttonMargins
        height: parent.height/10
    }
    onButtonBunkerReleased: {
        stackView.push(mainBunkerPage, StackView.Immediate);
    }
    onButtonMafiaReleased: {
        settingsButton.isBunkerStyle = false;
        soundButton.isBunkerStyle = false;
        stackView.push(mainMafiaPage, StackView.Immediate);
    }
}
