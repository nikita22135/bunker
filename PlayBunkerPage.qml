import QtQml.Models 2.2
import QtQml 2.2
import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.12

SomePage {
    backgroundColor: Qt.rgba(0,0,0,0.7)
    signal backButtonReleased();
    signal bunkerOnePhoneButtonReleased();
    signal bunkerNetworkButtonReleased();
    Image {
        id: bunkerPlayBackground
        source: "images/mainBackground.jpg"
        anchors.fill: parent
    }

    BunkerOnePhoneButton {
        id: bunkerOnePhoneButton
        anchors.centerIn: parent
        width: parent.width/1.3
        height: parent.height/10
        anchors.verticalCenterOffset: -parent.height/8
        onBunkerOnePhoneButtonReleased: {
            playBunkerPage.bunkerOnePhoneButtonReleased();
        }
    }
    BunkerNetworkButton {
        id: bunkerNetworkButton
        anchors.top: bunkerOnePhoneButton.bottom
        anchors.left: bunkerOnePhoneButton.left
        anchors.right: bunkerOnePhoneButton.right
        anchors.topMargin: mainBunkerPage.buttonMargins
        height: parent.height/10
        onBunkerNetworkButtonReleased: {
            playBunkerPage.bunkerNetworkButtonReleased();
        }
    }
    ButtonBack{
        id: bunkerBackButton
        buttonText: "Назад"
        anchors.top: bunkerNetworkButton.bottom
        anchors.left: bunkerNetworkButton.left
        anchors.right: bunkerNetworkButton.right
        anchors.topMargin: mainBunkerPage.buttonMargins
        height: parent.height/10
        onBackButtonReleased: {
            playBunkerPage.backButtonReleased();
        }
    }

    FooterImage {
        id: playBunkerFooterImage
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 1
        height: parent.height/12
    }

    onBackButtonReleased: {
        stackView.pop(StackView.Immediate);
    }

    onBunkerNetworkButtonReleased: {
        stackView.push(bunkerMultiplayerPage, StackView.Immediate);
    }

    onBunkerOnePhoneButtonReleased: {
        stackView.push(bunkerSoloGamePage, StackView.Immediate);
    }
}
