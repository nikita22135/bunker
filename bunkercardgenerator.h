#ifndef BUNKERCARDGENERATOR_H
#define BUNKERCARDGENERATOR_H

#include <QObject>

class QFile;

class BunkerCardGenerator : public QObject
{
    Q_OBJECT
public:
    explicit BunkerCardGenerator(QObject *parent = nullptr);

signals:
    void cardGenerated(QStringList cardList);
    void sendCatastropheParametersSignal(int catastropheId, QList<int> itemsList, int electricity, int peopleCount);
    void catastropheByParametersGenerated(QString catasropheLine);
    void newHealthGenerated(QString newHealth, int playerNum);
public slots:
    void generateCatastropheByParameters(int catastropheId, QList<int> itemsList, int electricity, int peopleCount);
    void generateRandomHealth(int playerNum);
public:
    Q_INVOKABLE QStringList generateCard();
    Q_INVOKABLE QString generateCatastrophe();
    Q_INVOKABLE void setId(int id);
    Q_INVOKABLE QString sendCatastropheParameters();

private:
    QFile* _bagFile;
    QFile* _healthFile;
    QFile* _hobbiesFile;
    QFile* _phobiasFile;
    QFile* _professionFile;
    QFile* _specialsFile;
    QFile* _maleCharacterFile;
    QFile* _femaleCharacterFile;
    QFile* _catastropheFile;
    QFile* _bunkerItemsFile;
    QList<int> _itemsList;
    int _electricity;
    int _catastropheId;
    int _peopleCount;
    int _id = 0;
    void findWeight(QString& str);
    QString simplifyLine(QStringList list, int& sum);
};

#endif // BUNKERCARDGENERATOR_H
