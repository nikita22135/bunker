import QtQml.Models 2.2
import QtQml 2.2
import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.12

SomePage {
    id: root
    anchors.fill: stackView
    visible: false
    signal updatePlayersInfo();
    signal setNicknames();
    signal sendNewHealth();
    property alias healthText: t13.text
    Image {
        id: bunkerGameBackground
        source: "images/mainBackground.jpg"
        anchors.fill: parent
    }
    SwipeView{
        id: view
        anchors.fill: parent
        currentIndex: 0
        Item {
            id: firstPage
            Rectangle{
                id: biologyRect1
                property alias acceptingRectVisibility: acceptingRect1.visible
                anchors.top: parent.top
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.topMargin: 3
                height: parent.height/9
                color: "#515151"
                border.width: 1
                border.color: "#202020"
                radius: 5

                Rectangle{
                    id: r11
                    anchors.left: parent.left
                    anchors.leftMargin: 3
                    anchors.top: parent.top
                    anchors.topMargin: 3
                    anchors.bottom: parent.bottom
                    anchors.bottomMargin: 3
                    width: r11.height
                    radius: r11.height/2
                    border.color: "#202020"
                    border.width: 1
                    color: "#909090"
                    Image {
                        id: i11
                        source: "images/sexIcon.png"
                        anchors.fill: parent
                        anchors.margins: 7
                    }
                }
                Text {
                    id: t11
                    text: window.cardText[0]
                    font.family: fontLoaderMaler.name
                    anchors.left: r11.right
                    anchors.leftMargin: 8
                    anchors.right: parent.right
                    anchors.rightMargin: 6
                    anchors.top: parent.top
                    anchors.bottom: parent.bottom
                    font.letterSpacing: 0.2
                    color: "#FFCC33"
                    wrapMode: Text.WordWrap
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignLeft
                    font.pointSize: 12
                }
                AcceptingRect {
                    id: acceptingRect1 //нужно после завершения игры ставить их на тру
                    property bool canBeAccepted: true
                    onAccepted: {
                        canBeAccepted = false;
                        biologyRect1.color = "#339933";
                        client.writeData("biology:"+window.cardText[0]+"\r\n"+"myId:"+myId+"\r\n");
                    }
                }

                MouseArea{
                    id: m1
                    anchors.fill:parent
                    onReleased: {
                        if (acceptingRect1.canBeAccepted)
                        {
                            acceptingRect1.visible = true;
                            //biologyRect1.acceptingRectVisibility = false;
                            professionRect1.acceptingRectVisibility = false;
                            hobbieRect1.acceptingRectVisibility = false;
                            phobiaRect1.acceptingRectVisibility = false;
                            characterRect1.acceptingRectVisibility = false;
                            bagRect1.acceptingRectVisibility = false;
                            healthRect1.acceptingRectVisibility = false;
                            specialRect1.acceptingRectVisibility = false;
                        }
                    }
                }
            }

            Rectangle {
                id: professionRect1
                property alias acceptingRectVisibility: acceptingRect2.visible
                anchors.top: biologyRect1.bottom
                anchors.topMargin: 2
                anchors.left: parent.left
                anchors.right: parent.right
                height: parent.height/9
                color: "#515151"
                border.width: 1
                border.color: "#202020"
                radius: 5

                Rectangle{
                    id: r12
                    anchors.left: parent.left
                    anchors.leftMargin: 3
                    anchors.top: parent.top
                    anchors.topMargin: 3
                    anchors.bottom: parent.bottom
                    anchors.bottomMargin: 3
                    width: r12.height
                    radius: r12.height/2
                    border.color: "#202020"
                    border.width: 1
                    color: "#909090"
                    Image {
                        id: i12
                        source: "images/professionIcon.png"
                        anchors.fill: parent
                        anchors.margins: 7
                    }
                }
                Text {
                    id: t12
                    
                    text: window.cardText[1]
                    font.family: fontLoaderMaler.name
                    anchors.left: r12.right
                    anchors.leftMargin: 8
                    anchors.right: parent.right
                    anchors.rightMargin: 6
                    anchors.top: parent.top
                    anchors.bottom: parent.bottom
                    font.letterSpacing: 0.2
                    color: "#FFCC33"
                    wrapMode: Text.WordWrap
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignLeft
                    font.pointSize: 12
                }
                AcceptingRect {
                    id: acceptingRect2
                    property bool canBeAccepted: true
                    onAccepted: {
                        canBeAccepted = false;
                        professionRect1.color = "#339933";
                        client.writeData("profession:"+window.cardText[1]+"\r\n"+"myId:"+myId+"\r\n");
                    }
                }

                MouseArea{
                    id: m2
                    anchors.fill:parent
                    onReleased: {
                        if (acceptingRect2.canBeAccepted)
                        {
                            acceptingRect2.visible = true;
                            biologyRect1.acceptingRectVisibility = false;
                            // professionRect1.acceptingRectVisibility = false;
                            hobbieRect1.acceptingRectVisibility = false;
                            phobiaRect1.acceptingRectVisibility = false;
                            characterRect1.acceptingRectVisibility = false;
                            bagRect1.acceptingRectVisibility = false;
                            healthRect1.acceptingRectVisibility = false;
                            specialRect1.acceptingRectVisibility = false;
                        }
                    }
                }
            }

            Rectangle {
                id: healthRect1
                property alias acceptingRectVisibility: acceptingRect3.visible
                anchors.top: professionRect1.bottom
                anchors.topMargin: 2
                anchors.left: parent.left
                anchors.right: parent.right
                height: parent.height/9
                color: "#515151"
                border.width: 1
                border.color: "#202020"
                radius: 5

                Rectangle{
                    id: r13
                    anchors.left: parent.left
                    anchors.leftMargin: 3
                    anchors.top: parent.top
                    anchors.topMargin: 3
                    anchors.bottom: parent.bottom
                    anchors.bottomMargin: 3
                    width: r13.height
                    radius: r13.height/2
                    border.color: "#202020"
                    border.width: 1
                    color: "#909090"
                    Image {
                        id: i13
                        source: "images/healthIcon.png"
                        anchors.fill: parent
                        anchors.margins: 7
                    }
                }
                Text {
                    id: t13
                    
                    text: window.cardText[2]
                    font.family: fontLoaderMaler.name
                    anchors.left: r13.right
                    anchors.leftMargin: 8
                    anchors.right: parent.right
                    anchors.rightMargin: 6
                    anchors.top: parent.top
                    anchors.bottom: parent.bottom
                    font.letterSpacing: 0.2
                    color: "#FFCC33"
                    wrapMode: Text.WordWrap
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignLeft
                    font.pointSize: 12
                }
                AcceptingRect {
                    id: acceptingRect3
                    property bool canBeAccepted: true
                    onAccepted: {
                        canBeAccepted = false;
                        healthRect1.color = "#339933";
                        client.writeData("health:"+window.cardText[2]+"\r\n"+"myId:"+myId+"\r\n");
                    }
                }

                MouseArea{
                    id: m3
                    anchors.fill:parent
                    onReleased: {
                        if (acceptingRect3.canBeAccepted)
                        {
                            acceptingRect3.visible = true;
                            biologyRect1.acceptingRectVisibility = false;
                            professionRect1.acceptingRectVisibility = false;
                            hobbieRect1.acceptingRectVisibility = false;
                            phobiaRect1.acceptingRectVisibility = false;
                            characterRect1.acceptingRectVisibility = false;
                            bagRect1.acceptingRectVisibility = false;
                            // healthRect1.acceptingRectVisibility = false;
                            specialRect1.acceptingRectVisibility = false;
                        }
                    }
                }
            }

            Rectangle {
                id: characterRect1
                property alias acceptingRectVisibility: acceptingRect4.visible
                anchors.top: healthRect1.bottom
                anchors.topMargin: 2
                anchors.left: parent.left
                anchors.right: parent.right
                height: parent.height/9
                color: "#515151"
                border.width: 1
                border.color: "#202020"
                radius: 5

                Rectangle{
                    id: r14
                    anchors.left: parent.left
                    anchors.leftMargin: 3
                    anchors.top: parent.top
                    anchors.topMargin: 3
                    anchors.bottom: parent.bottom
                    anchors.bottomMargin: 3
                    width: r14.height
                    radius: r14.height/2
                    border.color: "#202020"
                    border.width: 1
                    color: "#909090"
                    Image {
                        id: i14
                        source: "images/characterIcon.png"
                        anchors.fill: parent
                        anchors.margins: 7
                    }
                }
                Text {
                    id: t14
                    
                    text: window.cardText[3]
                    font.family: fontLoaderMaler.name
                    anchors.left: r14.right
                    anchors.leftMargin: 8
                    anchors.right: parent.right
                    anchors.rightMargin: 6
                    anchors.top: parent.top
                    anchors.bottom: parent.bottom
                    font.letterSpacing: 0.2
                    color: "#FFCC33"
                    wrapMode: Text.WordWrap
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignLeft
                    font.pointSize: 12
                }
                AcceptingRect {
                    id: acceptingRect4
                    property bool canBeAccepted: true
                    onAccepted: {
                        canBeAccepted = false;
                        characterRect1.color = "#339933";
                        client.writeData("character:"+window.cardText[3]+"\r\n"+"myId:"+myId+"\r\n");
                    }
                }

                MouseArea{
                    id: m4
                    anchors.fill:parent
                    onReleased: {
                        if (acceptingRect4.canBeAccepted)
                        {
                            acceptingRect4.visible = true;
                            biologyRect1.acceptingRectVisibility = false;
                            professionRect1.acceptingRectVisibility = false;
                            hobbieRect1.acceptingRectVisibility = false;
                            phobiaRect1.acceptingRectVisibility = false;
                            bagRect1.acceptingRectVisibility = false;
                            healthRect1.acceptingRectVisibility = false;
                            specialRect1.acceptingRectVisibility = false;
                        }
                    }
                }
            }

            Rectangle {
                id: hobbieRect1
                property alias acceptingRectVisibility: acceptingRect5.visible
                anchors.top: characterRect1.bottom
                anchors.topMargin: 2
                anchors.left: parent.left
                anchors.right: parent.right
                height: parent.height/9
                color: "#515151"
                border.width: 1
                border.color: "#202020"
                radius: 5

                Rectangle{
                    id: r15
                    anchors.left: parent.left
                    anchors.leftMargin: 3
                    anchors.top: parent.top
                    anchors.topMargin: 3
                    anchors.bottom: parent.bottom
                    anchors.bottomMargin: 3
                    width: r15.height
                    radius: r15.height/2
                    border.color: "#202020"
                    border.width: 1
                    color: "#909090"
                    Image {
                        id: i15
                        source: "images/hobbiesIcon.png"
                        anchors.fill: parent
                        anchors.margins: 7
                    }
                }
                Text {
                    id: t15
                    
                    text: window.cardText[4]
                    font.family: fontLoaderMaler.name
                    anchors.left: r15.right
                    anchors.leftMargin: 8
                    anchors.right: parent.right
                    anchors.rightMargin: 6
                    anchors.top: parent.top
                    anchors.bottom: parent.bottom
                    font.letterSpacing: 0.2
                    color: "#FFCC33"
                    wrapMode: Text.WordWrap
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignLeft
                    font.pointSize: 12
                }
                AcceptingRect {
                    id: acceptingRect5
                    property bool canBeAccepted: true
                    onAccepted: {
                        canBeAccepted = false;
                        hobbieRect1.color = "#339933";
                        client.writeData("hobbie:"+window.cardText[4]+"\r\n"+"myId:"+myId+"\r\n");
                    }
                }

                MouseArea{
                    id: m5
                    anchors.fill:parent
                    onReleased: {
                        if (acceptingRect5.canBeAccepted)
                        {
                            acceptingRect5.visible = true;
                            biologyRect1.acceptingRectVisibility = false;
                            professionRect1.acceptingRectVisibility = false;
                            //hobbieRect1.acceptingRectVisibility = false;
                            phobiaRect1.acceptingRectVisibility = false;
                            characterRect1.acceptingRectVisibility = false;
                            bagRect1.acceptingRectVisibility = false;
                            healthRect1.acceptingRectVisibility = false;
                            specialRect1.acceptingRectVisibility = false;
                        }
                    }
                }
            }

            Rectangle {
                id: phobiaRect1
                property alias acceptingRectVisibility: acceptingRect6.visible
                anchors.top: hobbieRect1.bottom
                anchors.topMargin: 2
                anchors.left: parent.left
                anchors.right: parent.right
                height: parent.height/9
                color: "#515151"
                border.width: 1
                border.color: "#202020"
                radius: 5

                Rectangle{
                    id: r16
                    anchors.left: parent.left
                    anchors.leftMargin: 3
                    anchors.top: parent.top
                    anchors.topMargin: 3
                    anchors.bottom: parent.bottom
                    anchors.bottomMargin: 3
                    width: r16.height
                    radius: r16.height/2
                    border.color: "#202020"
                    border.width: 1
                    color: "#909090"
                    Image {
                        id: i16
                        source: "images/phobiaIcon.png"
                        anchors.fill: parent
                        anchors.margins: 7
                    }
                }
                Text {
                    id: t16
                    text: window.cardText[5]
                    font.family: fontLoaderMaler.name
                    anchors.left: r16.right
                    anchors.leftMargin: 8
                    anchors.right: parent.right
                    anchors.rightMargin: 6
                    anchors.top: parent.top
                    anchors.bottom: parent.bottom
                    font.letterSpacing: 0.2
                    color: "#FFCC33"
                    wrapMode: Text.WrapAnywhere
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignLeft
                    font.pointSize: 12
                }
                AcceptingRect {
                    id: acceptingRect6
                    property bool canBeAccepted: true
                    onAccepted: {
                        canBeAccepted = false;
                        phobiaRect1.color = "#339933";
                        client.writeData("phobia:"+window.cardText[5]+"\r\n"+"myId:"+myId+"\r\n");
                    }
                }

                MouseArea{
                    id: m6
                    anchors.fill:parent
                    onReleased: {
                        if (acceptingRect6.canBeAccepted)
                        {
                            acceptingRect6.visible = true;
                            biologyRect1.acceptingRectVisibility = false;
                            professionRect1.acceptingRectVisibility = false;
                            hobbieRect1.acceptingRectVisibility = false;
                            // phobiaRect1.acceptingRectVisibility = false;
                            bagRect1.acceptingRectVisibility = false;
                            healthRect1.acceptingRectVisibility = false;
                            characterRect1.acceptingRectVisibility = false;
                            specialRect1.acceptingRectVisibility = false;
                        }
                    }
                }
            }

            Rectangle {
                id: bagRect1
                property alias acceptingRectVisibility: acceptingRect7.visible
                anchors.top: phobiaRect1.bottom
                anchors.topMargin: 2
                anchors.left: parent.left
                anchors.right: parent.right
                height: parent.height/9
                color: "#515151"
                border.width: 1
                border.color: "#202020"
                radius: 5

                Rectangle{
                    id: r17
                    anchors.left: parent.left
                    anchors.leftMargin: 3
                    anchors.top: parent.top
                    anchors.topMargin: 3
                    anchors.bottom: parent.bottom
                    anchors.bottomMargin: 3
                    width: r17.height
                    radius: r17.height/2
                    border.color: "#202020"
                    border.width: 1
                    color: "#909090"
                    Image {
                        id: i17
                        source: "images/bagIcon.png"
                        anchors.fill: parent
                        anchors.margins: 7
                    }
                }
                Text {
                    id: t17
                    
                    text: window.cardText[6]
                    font.family: fontLoaderMaler.name
                    anchors.left: r17.right
                    anchors.leftMargin: 8
                    anchors.right: parent.right
                    anchors.rightMargin: 6
                    anchors.top: parent.top
                    anchors.bottom: parent.bottom
                    font.letterSpacing: 0.2
                    color: "#FFCC33"
                    wrapMode: Text.WrapAnywhere
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignLeft
                    font.pointSize: 12
                }
                AcceptingRect {
                    id: acceptingRect7
                    property bool canBeAccepted: true
                    onAccepted: {
                        canBeAccepted = false;
                        bagRect1.color = "#339933";
                        client.writeData("bag:"+window.cardText[6]+"\r\n"+"myId:"+myId+"\r\n");
                    }
                }

                MouseArea{
                    id: m7
                    anchors.fill:parent
                    onReleased: {
                        if (acceptingRect7.canBeAccepted)
                        {
                            acceptingRect7.visible = true;
                            biologyRect1.acceptingRectVisibility = false;
                            professionRect1.acceptingRectVisibility = false;
                            hobbieRect1.acceptingRectVisibility = false;
                            phobiaRect1.acceptingRectVisibility = false;
                            //bagRect1.acceptingRectVisibility = false;
                            healthRect1.acceptingRectVisibility = false;
                            characterRect1.acceptingRectVisibility = false;
                            specialRect1.acceptingRectVisibility = false;
                        }
                    }
                }
            }

            Rectangle {
                id: specialRect1
                property alias acceptingRectVisibility: acceptingRect8.visible
                anchors.top: bagRect1.bottom
                anchors.topMargin: 2
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 4
                color: "#515151"
                border.width: 1
                border.color: "#202020"
                radius: 5

                Rectangle{
                    id: r18
                    anchors.left: parent.left
                    anchors.leftMargin: 3
                    anchors.top: parent.top
                    anchors.topMargin: 3
                    height: 45
                    width: r18.height
                    radius: r18.height/2
                    border.color: "#202020"
                    border.width: 1
                    color: "#909090"
                    Image {
                        id: i18
                        source: "images/specialsIcon.png"
                        anchors.fill: parent
                        anchors.topMargin: 4
                        anchors.bottomMargin: 7
                        anchors.leftMargin: 7
                        anchors.rightMargin: 7
                    }
                }
                Text {
                    id: t18
                    text: window.cardText[7]
                    font.family: fontLoaderMaler.name
                    anchors.left: r18.right
                    anchors.leftMargin: 8
                    anchors.right: parent.right
                    anchors.rightMargin: 6
                    anchors.top: parent.top
                    anchors.bottom: parent.bottom
                    font.letterSpacing: 0.2
                    color: "#FFCC33"
                    wrapMode: Text.WordWrap
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignLeft
                    font.pointSize: 8
                }
                AcceptingRect {
                    id: acceptingRect8
                    property bool canBeAccepted: true
                    rectHeight: parent.height/1.5
                    onAccepted: {
                        canBeAccepted = false;
                        specialRect1.color = "#339933";
                        client.writeData("special1:"+window.cardText[7]+"\r\n"+"myId:"+myId+"\r\n");
                        if (window.cardText[7] === "Вы меняете характеристику здоровье у одного из игроков на случайную" ||
                                window.cardText[7] === "Вы меняете характеристику багаж у одного из игроков на случайную" ||
                                window.cardText[7] === "Вы меняете характеристику профессия у одного из игроков на случайную" ||
                                window.cardText[7] === "Вы меняете характеристику фобия у одного из игроков на случайную" ||
                                window.cardText[7] === "Вы меняете характеристику хобби у одного из игроков на случайную" ||
                                window.cardText[7] === "Вы можете дать иммунитет от голосования на один круг любому игроку, кроме себя" ||
                                window.cardText[7] === "Поменяйтесь характеристикой здоровье с выбранным игроком" ||
                                window.cardText[7] === "Поменяйтесь характеристикой хобби с выбранным игроком" ||
                                window.cardText[7] === "Поменяйтесь характеристикой специальность с выбранным игроком" ||
                                window.cardText[7] === "Поменяйтесь характеристикой фобия с выбранным игроком" ||
                                window.cardText[7] === "Вы даете выбранному игроку ребенка, он обязательно должен попасть в бункер с этим игроком, ребенок занимает 1 место" ||
                                window.cardText[7] === "Поменяйтесь характеристикой багаж с выбранным игроком" ||
                                window.cardText[7] === "Вы можете заставить выбранного игрока вскрыть характеристику здоровье" ||
                                window.cardText[7] === "Вы можете заставить выбранного игрока вскрыть характеристику багаж" ||
                                window.cardText[7] === "Вы можете заставить выбранного игрока вскрыть характеристику хобби" ||
                                window.cardText[7] === "Вы можете заставить выбранного игрока вскрыть характеристику фобия" ||
                                window.cardText[7] === "Вы можете заставить выбранного игрока вскрыть характеристику специальность" ||
                                window.cardText[7] === "Вы можете убрать у игрока характеристику чайдфри")
                        {
                            choosePlayerRect.visible = true;
                        }
                    }
                }

                MouseArea{
                    id: m8
                    anchors.fill:parent
                    onReleased: {
                        if (acceptingRect8.canBeAccepted)
                        {
                            acceptingRect8.visible = true;
                            biologyRect1.acceptingRectVisibility = false;
                            professionRect1.acceptingRectVisibility = false;
                            hobbieRect1.acceptingRectVisibility = false;
                            phobiaRect1.acceptingRectVisibility = false;
                            bagRect1.acceptingRectVisibility = false;
                            healthRect1.acceptingRectVisibility = false;
                            characterRect1.acceptingRectVisibility = false;
                            // specialRect1.acceptingRectVisibility = false;
                        }
                    }
                }
            }
            Rectangle{
                id: choosePlayerRect
                signal setNicknames();
                visible: false
                anchors.fill: parent
                color: Qt.rgba(0,0,0,0.75)
                Text {
                    id: choosePlayerText
                    text: qsTr("Выберите игрока")
                    font.family: fontLoaderMaler.name
                    font.pointSize: 14
                    anchors.top: parent.top
                    anchors.topMargin: 2
                    color: "#FFCC33"
                    //anchors.horizontalCenter: parent.horizontalCenter
                    anchors.right: parent.right
                    anchors.left: parent.left
                    height: 40
                    wrapMode: Text.WordWrap
                    horizontalAlignment: Text.AlignHCenter
                }
                ListView{
                    id: playersListView
                    anchors.top: choosePlayerText.bottom
                    anchors.left: parent.left
                    anchors.right: parent.right
                    anchors.bottom: parent.bottom
                    delegate:
                        Rectangle{
                        id: somePlayerRect
                        width: parent.width
                        height: 45
                        color: "#515151"
                        border.width: 1
                        border.color: "#202020"
                        radius: 5
                        Text {
                            text: nick
                            font.pointSize: 12
                            font.bold: true
                            font.family: fontLoaderMaler.name
                            color: "#FFCC33"
                            anchors.centerIn: parent
                        }
                        MouseArea{
                            id: somePlayerMouseArea
                            anchors.fill: parent
                            onReleased: {
                                playerChoosingAcceptingRect.visible = true;
                            }
                        }
                        AcceptingRect{
                            id: playerChoosingAcceptingRect
                            titleText: "Выбрать этого игрока?"
                            onAccepted: {
                                choosePlayerRect.visible = false;
                                if (window.cardText[7] === "Вы меняете характеристику здоровье у одного из игроков на случайную")
                                {
                                    client.writeData("changeHealthOfPlayerWithNum:"+num+"\r\n"+"myId:"+window.myId+"\r\n");
                                    console.log(num);
                                }
                                else if (window.cardText[7] === "Вы меняете характеристику багаж у одного из игроков на случайную")
                                    client.writeData("changeBagOfPlayerWithNum:"+num+"\r\n"+"myId:"+window.myId+"\r\n");
                                else if (window.cardText[7] === "Вы меняете характеристику профессия у одного из игроков на случайную")
                                    client.writeData("changeProfessionOfPlayerWithNum:"+num+"\r\n"+"myId:"+window.myId+"\r\n");
                                else if (window.cardText[7] === "Вы меняете характеристику фобия у одного из игроков на случайную")
                                    client.writeData("changePhobiaOfPlayerWithNum:"+num+"\r\n"+"myId:"+window.myId+"\r\n");
                                else if (window.cardText[7] === "Вы меняете характеристику хобби у одного из игроков на случайную")
                                    client.writeData("changeHobbieOfPlayerWithNum:"+num+"\r\n"+"myId:"+window.myId+"\r\n");
                                else if (window.cardText[7] === "Вы можете дать иммунитет от голосования на один круг любому игроку, кроме себя")
                                    client.writeData("giveImmuneOfPlayerWithNum:"+num+"\r\n"+"myId:"+window.myId+"\r\n");
                                else if (window.cardText[7] === "Поменяйтесь характеристикой здоровье с выбранным игроком")
                                    client.writeData("swapHealthWithPlayerWithNum:"+num+"\r\n"+"myId:"+window.myId+"\r\n");
                                else if (window.cardText[7] === "Поменяйтесь характеристикой хобби с выбранным игроком")
                                    client.writeData("swapHobbieWithPlayerWithNum:"+num+"\r\n"+"myId:"+window.myId+"\r\n");
                                else if (window.cardText[7] === "Поменяйтесь характеристикой специальность с выбранным игроком")
                                    client.writeData("swapProfessionWithPlayerWithNum:"+num+"\r\n"+"myId:"+window.myId+"\r\n");
                                else if (window.cardText[7] === "Поменяйтесь характеристикой фобия с выбранным игроком")
                                    client.writeData("swapPhobiaWithPlayerWithNum:"+num+"\r\n"+"myId:"+window.myId+"\r\n");
                                else if (window.cardText[7] === "Вы даете выбранному игроку ребенка, он обязательно должен попасть в бункер с этим игроком, ребенок занимает 1 место")
                                    client.writeData("giveChildToPlayerWithNum:"+num+"\r\n"+"myId:"+window.myId+"\r\n");
                                else if (window.cardText[7] === "Поменяйтесь характеристикой багаж с выбранным игроком")
                                    client.writeData("swapBagWithPlayerWithNum:"+num+"\r\n"+"myId:"+window.myId+"\r\n");
                                else if (window.cardText[7] === "Вы можете заставить выбранного игрока вскрыть характеристику здоровье")
                                    client.writeData("forceOpenHealthOfPlayerWithNum:"+num+"\r\n"+"myId:"+window.myId+"\r\n");
                                else if (window.cardText[7] === "Вы можете заставить выбранного игрока вскрыть характеристику багаж")
                                    client.writeData("forceOpenBagOfPlayerWithNum:"+num+"\r\n"+"myId:"+window.myId+"\r\n");
                                else if (window.cardText[7] === "Вы можете заставить выбранного игрока вскрыть характеристику хобби")
                                    client.writeData("forceOpenHobbieOfPlayerWithNum:"+num+"\r\n"+"myId:"+window.myId+"\r\n");
                                else if (window.cardText[7] === "Вы можете заставить выбранного игрока вскрыть характеристику фобия")
                                    client.writeData("forceOpenPhobiaOfPlayerWithNum:"+num+"\r\n"+"myId:"+window.myId+"\r\n");
                                else if (window.cardText[7] === "Вы можете заставить выбранного игрока вскрыть характеристику специальность")
                                    client.writeData("forceOpenProfessionOfPlayerWithNum:"+num+"\r\n"+"myId:"+window.myId+"\r\n");
                                else if (window.cardText[7] === "Вы можете убрать у игрока характеристику чайдфри")
                                    client.writeData("removeChildfreeOfPlayerWithNum:"+num+"\r\n"+"myId:"+window.myId+"\r\n");
                            }
                        }
                    }
                    model: ChoosePlayersListModel {id: choosePlayersListModel1}
                }
                ChoosePlayersListModel {
                    id: choosePlayersListModel
                }
                onSetNicknames: {
                   // choosePlayersListModel1.clear();
                    for (var i = 0; i<16; i++)
                    {
                        if (nicknames[i])
                        {
                            choosePlayersListModel1.append({"nick": nicknames[i],
                                                           "num": i})
                        }
                    }
                }
            }
        }
        Item {
            id: secondPage
            property int initialY: catastropheText1.y
            Rectangle {
                id: bunkerCatastropheBackground
                color: "#515151"
                anchors.fill: parent
            }
            Text {
                id: catastropheText1
                text: window.catastropheText
                anchors.right: parent.right
                anchors.rightMargin: 6
                anchors.left: parent.left
                anchors.leftMargin: 6
                y: 2
                font.letterSpacing: 0.05
                color: "#FFFFFF"
                wrapMode: Text.WordWrap
                font.pointSize: 12
            }

            Timer {
                interval: 10;
                running: true;
                repeat: true
                onTriggered: secondPage.initialY = catastropheTextMouseArea1.mouseY;
            }
            MouseArea{
                id: catastropheTextMouseArea1
                anchors.fill: parent
                hoverEnabled: true

                onPositionChanged: {
                    if ((catastropheTextMouseArea1.mouseY > secondPage.initialY)&&(catastropheText1.y<2)){
                        catastropheText1.y += 7;
                    }
                    else if ((catastropheTextMouseArea1.mouseY<secondPage.initialY)&&(catastropheText1.y>-500))
                    {
                        catastropheText1.y -= 7;
                    }
                }
            }
        }
        Item {
            id: thirdPage
            signal updatePlayersInfo();
            ListView{
                id: somePlayerListView
                signal updatePlayersInfo();
                anchors.fill: parent
                delegate:
                    Rectangle{
                    id: feautureRect
                    width: root.width
                    height: hght
                    color: "#515151"
                    border.width: 1
                    border.color: "#202020"
                    radius: 5
                    Rectangle{
                        id: imgRect
                        anchors.left: parent.left
                        anchors.leftMargin: 3
                        anchors.top: parent.top
                        anchors.topMargin: 3
                        anchors.bottom: (feautureRect.height == somePlayerListView.height/9) ? parent.bottom : 0
                        anchors.bottomMargin: 3
                        height: 45
                        width: imgRect.height
                        radius: imgRect.height/2
                        border.color: "#202020"
                        border.width: 1
                        color: "#909090"
                        Image {
                            id: icon
                            source: img
                            anchors.fill: parent
                            anchors.margins: 7
                        }
                    }

                    Text {
                        id: feautureText
                        text: playerFeauture
                        font.pointSize: fnt
                        font.bold: true
                        font.family: fontLoaderMaler.name
                        color: "#FFCC33"
                        wrapMode: Text.WrapAnywhere
                        anchors.left: imgRect.right
                        anchors.leftMargin: 8
                        anchors.right: parent.right
                        anchors.rightMargin: 6
                        anchors.top: parent.top
                        anchors.bottom: parent.bottom
                        font.letterSpacing: 0.2
                        verticalAlignment: Text.AlignVCenter
                        horizontalAlignment: Text.AlignLeft
                    }
                }
                onUpdatePlayersInfo: {
                    somePlayerListModel1.clear();
                    for (var i = 0; i<16; i++)
                    {
                        if (nicknames[i])
                        {
                            for (var j = 0; j<9; j++)
                            {
                                switch (j)
                                {
                                case 0:

                                    somePlayerListModel1.append({"playerFeauture": nicknames[i],
                                                                    "img": "images/avatar1.png",
                                                                    "hght": somePlayerListView.height/9,
                                                                    "fnt": 12});
                                    if (i === 0) //надо подумать как это сделать
                                    {
                                        somePlayerListView.spacing = 0;
                                    }
                                    else
                                    {
                                        somePlayerListView.spacing = 5;
                                    }
                                    break;
                                case 1:
                                    somePlayerListModel1.append({"playerFeauture": biology[i],
                                                                    "img": "images/sexIcon.png",
                                                                    "hght": somePlayerListView.height/9,
                                                                    "fnt": 12})
                                    somePlayerListView.spacing = 0;
                                    break;
                                case 2:
                                    somePlayerListModel1.append({"playerFeauture": profession[i],
                                                                    "img": "images/professionIcon.png",
                                                                    "hght": somePlayerListView.height/9,
                                                                    "fnt": 12})
                                    somePlayerListView.spacing = 0;
                                    break;
                                case 3:
                                    somePlayerListModel1.append({"playerFeauture": health[i],
                                                                    "img": "images/healthIcon.png",
                                                                    "hght": somePlayerListView.height/9,
                                                                    "fnt": 12})
                                    somePlayerListView.spacing = 0;
                                    break;
                                case 4:
                                    somePlayerListModel1.append({"playerFeauture": charater[i],
                                                                    "img": "images/characterIcon.png",
                                                                    "hght": somePlayerListView.height/9,
                                                                    "fnt": 12})
                                    somePlayerListView.spacing = 0;
                                    break;
                                case 5:
                                    somePlayerListModel1.append({"playerFeauture": hobbie[i],
                                                                    "img": "images/hobbiesIcon.png",
                                                                    "hght": somePlayerListView.height/9,
                                                                    "fnt": 12})
                                    somePlayerListView.spacing = 0;
                                    break;
                                case 6:
                                    somePlayerListModel1.append({"playerFeauture": phobia[i],
                                                                    "img": "images/phobiaIcon.png",
                                                                    "hght": somePlayerListView.height/9,
                                                                    "fnt": 12})
                                    somePlayerListView.spacing = 0;
                                    break;
                                case 7:
                                    somePlayerListModel1.append({"playerFeauture": bag[i],
                                                                    "img": "images/bagIcon.png",
                                                                    "hght": somePlayerListView.height/9,
                                                                    "fnt": 12})
                                    somePlayerListView.spacing = 0;
                                    break;
                                case 8:
                                    somePlayerListModel1.append({"playerFeauture": special[i],
                                                                    "img": "images/specialsIcon.png",
                                                                    "hght": somePlayerListView.height/6,
                                                                    "fnt": 8})
                                    break;
                                }
                            }
                        }
                    }
                }
                model: SomePlayerListModel{id: somePlayerListModel1}
            }
            SomePlayerListModel {
                id: somePlayerListModel
            }
            onUpdatePlayersInfo: {
                somePlayerListView.updatePlayersInfo();
            }

        }
    }
    PageIndicator {
        id: indicator

        count: view.count
        currentIndex: view.currentIndex

        anchors.bottom: view.bottom
        anchors.horizontalCenter: parent.horizontalCenter
    }
    onUpdatePlayersInfo:{
        thirdPage.updatePlayersInfo();
    }
    onSetNicknames:{
        choosePlayerRect.setNicknames();
    }
    onSendNewHealth:{
        if (!acceptingRect3.canBeAccepted)
            client.writeData("health:"+window.cardText[2]+"\r\n"+"myId:"+myId+"\r\n");
    }
}
