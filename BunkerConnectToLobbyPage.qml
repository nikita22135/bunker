import QtQml.Models 2.2
import QtQml 2.2
import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.12

SomePage {
    id: root
    signal bunkerHostsChanged();
    property alias isTimerRunning: updateBunkerHostsTimer.running //надо ставить эту штуку false на любой другой странице
    property alias tooManyPlayersToConnectMessageVisibility: tooManyPlayersToConnect.visible
    property alias currentIndex: bunkerHostsListView.currentIndex
    Image {
        id: bunkerConnectToLobbyBackground
        source: "images/mainBackground.jpg"
        anchors.fill: parent
    }
    Rectangle {
        id: tooManyPlayersToConnect
        anchors.fill: parent;
        color: Qt.rgba(20,20,20,0.5);
        visible: false
        z: 10
        Text {
            id: tooManyPlayersToConnectMessageText
            text: qsTr("Лобби полностью заполнено");
            font.bold: true
            font.pointSize: 16
            anchors.centerIn: parent
            width: parent.width - 10
            wrapMode: Text.WordWrap
            font.family: fontLoaderMaler.name
        }
        Rectangle{
            id: okButton1
            signal okButton1Released();
            anchors.bottom: parent.bottom
            anchors.horizontalCenter: parent.horizontalCenter
            width: parent.width/1.4
            height: parent.height/10
            anchors.bottomMargin: 7
            color: okButton1MouseArea.containsPress? Qt.darker(mainBunkerPage.buttonColor, 1.1) : mainBunkerPage.buttonColor
            radius: 15
            border.width: 2
            border.color: mainBunkerPage.buttonBorderColor

            Text {
                id: okButtonText1
                font.family: fontLoaderMaler.name
                text: qsTr("ОК")
                anchors.centerIn: parent
                font.pointSize: 14
            }
            MouseArea{
                id: okButton1MouseArea
                anchors.fill:parent
                onReleased: {
                    okButton1.okButton1Released();
                }
            }

            onOkButton1Released: {
                root.tooManyPlayersToConnectMessageVisibility = false;
            }
        }
    }

    ListView {
        id: bunkerHostsListView
        property int currentIndex: 0
        anchors.fill: parent
        delegate:
            Rectangle {
            width: parent.width
            height: 45
            color: "#515151"
            border.width: 1
            border.color: "#202020"
            radius: 5
            Text {
                id: bunkerLobbyNickAndOtherText
                text: nick+" "+playerCount+"/16"
                font.pointSize: 12
                font.bold: true
                font.family: fontLoaderMaler.name
                color: "#FFCC33"
                anchors.centerIn: parent
            }
            Image {
                id: bunkerLobbyIsLock
                source: isPass? "images/lockIcon.png" : "images/unlockIcon.png"
                anchors.right: parent.right
                anchors.top: parent.top
                anchors.bottom: parent.bottom
                anchors.margins: 8
                width: bunkerLobbyIsLock.height
            }
            MouseArea
            {
                id: bunkerConnectMouseArea
                property int viewCurrentIndex: currentIndex
                anchors.fill:parent
                onReleased: {
                    bunkerHostsListView.currentIndex = viewCurrentIndex;
                    if (!isPass)
                        client.writeData("connectToBunkerHostWithId:"+viewCurrentIndex
                                         +"\r\n"+"myId:"+myId+"\r\n"+"myNickname:"+nickname+"\r\n"+
                                         "myPassword:none"+"\r\n");
                    else
                    {
                        bunkerPasswordToConnect.visible = true;
                    }
                }
            }
        }
        model: BunkerHostsListModel { id: bunkerHostsListModel1 }
    }

    BunkerHostsListModel {
        id: bunkerHostsListModel
    }

    Timer {
        id: updateBunkerHostsTimer
        interval: 10; running: false; repeat: true
        onTriggered: {
            client.writeData("showAvailableBunkerHosts\n");
            updateBunkerHostsTimer.interval = 2000;
        }
    }

    onBunkerHostsChanged: {
        bunkerHostsListModel1.clear();
        for (var i = 0; i<64; i++)
        {
            if (bunkerHostsArr[i])
            {
                bunkerHostsListModel1.append(
                            {
                                "nick": bunkerHostsArr[i],
                                "currentIndex": bunkerHostsIdArr[i],
                                "playerCount": bunkerPlayersCountList[i],
                                "isPass": bunkerIsPassList[i]
                            })
            }
        }
    }
    Rectangle{
        id: bunkerPasswordToConnect
        anchors.fill: parent
        color: Qt.rgba(20,20,20,0.5);
        visible: false
        TextField{
            id: bunkerPassToConnectField
            objectName: "bunkerPassToConnectField"
            anchors.centerIn: parent
            width: parent.width - 14
            height: 40
            font.pointSize: 14
            color: "#FFCC33"
            background: Rectangle {
                color: bunkerPasswordToConnect.focus ? "#AAAAAA" : "#515151"
                border.color: "#353535"
                border.width: 1
                radius: 7
            }
        }
        Text {
            id: bunkerPassToConnectText
            text: qsTr("Пароль")
            width: bunkerMultiplayerPage.width/3
            height: 40
            font.family: fontLoaderMaler.name
            font.pointSize: 14
            anchors.bottom: bunkerPassToConnectField.top
            anchors.horizontalCenter: parent.horizontalCenter
        }
        Rectangle{
            id: okButton3
            signal okButton3Released();
            anchors.bottom: parent.bottom
            anchors.horizontalCenter: parent.horizontalCenter
            width: parent.width/1.4
            height: parent.height/10
            anchors.bottomMargin: 7
            color: okButton3MouseArea.containsPress? Qt.darker(mainBunkerPage.buttonColor, 1.1) : mainBunkerPage.buttonColor
            radius: 15
            border.width: 2
            border.color: mainBunkerPage.buttonBorderColor
            Text {
                id: okButton3Text
                font.family: fontLoaderMaler.name
                text: qsTr("ОК")
                anchors.centerIn: parent
                font.pointSize: 14
            }
            MouseArea{
                id: okButton3MouseArea
                anchors.fill: parent
                hoverEnabled: true
                onReleased: {
                    okButton3.okButton3Released();
                }
            }
            onOkButton3Released:{
                bunkerPasswordToConnect.visible = false;
                client.writeData("connectToBunkerHostWithId:"+root.currentIndex
                                 +"\r\n"+"myId:"+myId+"\r\n"+"myNickname:"+nickname+"\r\n"+
                                 "myPassword:"+bunkerPassToConnectField.text+"\r\n");
            }
        }
    }
    Rectangle{
        id: bunkerPasswordIsNotMatch
        anchors.fill: parent
        color: Qt.rgba(20,20,20,0.5);
        visible: false
        Text {
            id: bunkerPasswordIsNotMatchText
            text: qsTr("Неправильный пароль")
            wrapMode: Text.WordWrap
            width: bunkerMultiplayerPage.width/3
            height: 40
            font.family: fontLoaderMaler.name
            font.pointSize: 14
            anchors.centerIn: parent
        }
        Rectangle{
            id: okButton4
            signal okButton4Released();
            anchors.bottom: parent.bottom
            anchors.horizontalCenter: parent.horizontalCenter
            width: parent.width/1.4
            height: parent.height/10
            anchors.bottomMargin: 7
            color: okButton4MouseArea.containsPress? Qt.darker(mainBunkerPage.buttonColor, 1.1) : mainBunkerPage.buttonColor
            radius: 15
            border.width: 2
            border.color: mainBunkerPage.buttonBorderColor
            Text {
                id: okButton4Text
                font.family: fontLoaderMaler.name
                text: qsTr("ОК")
                anchors.centerIn: parent
                font.pointSize: 14
            }
            MouseArea{
                id: okButton4MouseArea
                anchors.fill: parent
                onReleased: {
                    okButton4.okButton4Released();
                }
            }
            onOkButton4Released:{
                bunkerPasswordIsNotMatch.visible = false;
            }
        }
    }
}
