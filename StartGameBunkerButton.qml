import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.12

Rectangle{
    id: startGameBunkerButton
    property alias startBunkerGameText: startBunkerGameText.text
    radius: 5
    border.width: 2
    border.color: mainBunkerPage.buttonBorderColor
    color: startGameBunkerButtonMouseArea.containsPress? Qt.darker(mainBunkerPage.buttonColor, 1.1) : mainBunkerPage.buttonColor
    signal startGameBunkerButtonReleased();
    Text {
        id: startBunkerGameText
        text: qsTr("Начать игру")
        anchors.centerIn: parent
        font.family: fontLoaderMaler.name
        color: "#000000"
        font.letterSpacing: 0.3
        font.pointSize: 14
    }
    MouseArea{
        id: startGameBunkerButtonMouseArea
        anchors.fill: parent
        hoverEnabled: true
        onReleased: {
            startGameBunkerButton.startGameBunkerButtonReleased();
        }
    }
}
