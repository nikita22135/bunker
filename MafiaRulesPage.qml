import QtQml.Models 2.2
import QtQml 2.2
import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.12

SomePage {
    id: root
    signal backButtonClicked();
    property int initialY: rulesText.y
    backgroundColor: Qt.rgba(0,0,0,0.7)
    Rectangle
    {
        color: "#515151"
        anchors.fill: parent
    }

    Rectangle{
        id: rulesRect
        color: Qt.rgba(0,0,0,0)
        anchors.fill: parent
        FontLoader{
            id: fontLoader2
            source: "fonts/Roboto-Regular.ttf"
        }

        BackButtonMafia {
            id: backButton
            anchors.left: parent.left
            anchors.leftMargin: 7
            anchors.top: parent.top
            anchors.topMargin: 7
            width: 30
            height: 20
        }

        Text {
            id: headerText
            anchors.horizontalCenter: parent.horizontalCenter
            y:7
            color: "#FFFFFF"
            text: qsTr("СЮЖЕТ ИГРЫ\n")
            font.pointSize: 12
        }

        Text {
            id: rulesText
            anchors.right: parent.right
            anchors.rightMargin: 6
            anchors.left: parent.left
            anchors.leftMargin: 6
            y: 30
            font.letterSpacing: 0.05
            color: "#FFFFFF"
            wrapMode: Text.WordWrap
            font.pointSize: 12
            text: qsTr("Правил нет, но вы держитесь.")
        }
    }

    Timer {
        interval: 10;
        running: true;
        repeat: true
        onTriggered: root.initialY = textMouseArea.mouseY;
    }
    MouseArea{
        id: textMouseArea
        anchors.top: parent.top
        anchors.topMargin: 30
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        hoverEnabled: true

        onPositionChanged: {
            if ((textMouseArea.mouseY > root.initialY)&&(rulesText.y<30)){
                rulesText.y += 7;
                headerText.y += 7;
            }
            else if ((textMouseArea.mouseY<root.initialY)&&(rulesText.y>-500))
            {
                rulesText.y -= 7;
                headerText.y -= 7;
            }
        }
    }
    onBackButtonClicked: {
        stackView.pop(StackView.Immediate);
        settingsButtonVisibility = true;
        soundButtonVisibility = true;
        rulesText.y = 30;
        headerText.y = 7;
    }
}
