#include "bunkercardgenerator.h"
#include "bunkerclient.h"
#include "settings.h"
#include <QFontDatabase>
#include <QGuiApplication>
#include <QIcon>
#include <QQmlApplicationEngine>
#include <QQmlContext>

int main(int argc, char* argv[])
{
    //qputenv("QML_DISABLE_DISTANCEFIELD", "1");
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;
    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(
        &engine, &QQmlApplicationEngine::objectCreated, &app,
        [url](QObject* obj, const QUrl& objUrl) {
            if (!obj && url == objUrl)
                QCoreApplication::exit(-1);
        },
        Qt::QueuedConnection);
    app.setWindowIcon(QIcon(":/images/icon.png"));

    //int fontId = QFontDatabase::addApplicationFont(":/fonts/Maler.ttf");
    BunkerCardGenerator gen;
    BunkerClient client;
    Settings settings;
    QObject::connect(&gen,&BunkerCardGenerator::sendCatastropheParametersSignal,&client,&BunkerClient::sendCatastropheParametersSlot);
    QObject::connect(&client,&BunkerClient::sendCatastropheParameters,&gen,&BunkerCardGenerator::generateCatastropheByParameters);
    QObject::connect(&client,&BunkerClient::changeHealthToRandom,&gen,&BunkerCardGenerator::generateRandomHealth);
    //сюда нужен коннект
    engine.rootContext()->setContextProperty("client", &client);
    engine.rootContext()->setContextProperty("settings", &settings);
    engine.rootContext()->setContextProperty("gen", &gen);
    engine.load(url);

    return app.exec();
}
