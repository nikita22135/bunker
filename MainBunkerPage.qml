import QtQml.Models 2.2
import QtQml 2.2
import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.12

SomePage {
    id: root
    backgroundColor: Qt.rgba(0,0,0,0.7)
    property color buttonColor: "#FFCC33"
    property color buttonBorderColor: "#333333"
    property int buttonMargins: 10
    signal buttonRulesReleased();
    signal backButtonReleased();
    signal playBunkerButtonReleased();
    Image {
        id: bunkerBackground
        source: "images/mainBackground.jpg"
        anchors.fill: parent
    }
    ButtonPlay {
        id: buttonPlay
        anchors.centerIn: parent
        width: parent.width/1.4
        height: parent.height/10
        anchors.verticalCenterOffset: -parent.height/8
        onPlayButtonReleased: {
            root.playBunkerButtonReleased();
        }
    }
    ButtonRules {
        id: buttonRules
        anchors.top: buttonPlay.bottom
        anchors.left: buttonPlay.left
        anchors.right: buttonPlay.right
        height: parent.height/10
        anchors.topMargin: root.buttonMargins
        onRulesButtonReleased: {
            root.buttonRulesReleased();
        }
    }
    ButtonBack {
        id: buttonBack
        buttonText: "Назад"
        anchors.top: buttonRules.bottom
        anchors.left: buttonRules.left
        anchors.right: buttonRules.right
        anchors.topMargin: root.buttonMargins
        height: parent.height/10
        onBackButtonReleased: {
            root.backButtonReleased();
        }
    }
    FooterImage {
        id: footerImage
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 1
        height: parent.height/12
    }
    onButtonRulesReleased: {
        stackView.push(bunkerRulesPage, StackView.Immediate);
        settingsButtonVisibility = false;
        soundButtonVisibility = false;
        bunkerRulesPage.forceActiveFocus();
    }
    onBackButtonReleased: {
        stackView.pop(StackView.Immediate);
    }
    onPlayBunkerButtonReleased: {
        stackView.push(playBunkerPage, StackView.Immediate);
    }
}
