import QtQml.Models 2.2
import QtQml 2.2
import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.12

SomePage {
    id: root
    signal backButtonReleased();
    signal buttonBunkerGenerateCardReleased();
    signal buttonBunkerGenerateCatastropheReleased();
    Image {
        id: bunkerSoloGameBackground
        source: "images/mainBackground.jpg"
        anchors.fill: parent
    }

    ButtonGenerateCard {
        id: buttonGenerateCard
        anchors.centerIn: parent
        width: parent.width/1.3
        height: parent.height/7
        anchors.verticalCenterOffset: -parent.height/6
        onButtonBunkerGenerateCardReleased: {
            root.buttonBunkerGenerateCardReleased();
        }
    }

    ButtonGenerateCatastrophe {
        id: buttonGenerateCatastrophe
        anchors.top: buttonGenerateCard.bottom
        anchors.left: buttonGenerateCard.left
        anchors.right: buttonGenerateCard.right
        anchors.topMargin: mainBunkerPage.buttonMargins
        height: parent.height/7
        onButtonBunkerGenerateCatastropheReleased: {
            root.buttonBunkerGenerateCatastropheReleased();
        }
    }

    ButtonBack{
        id: buttonSoloGameBack
        anchors.top: buttonGenerateCatastrophe.bottom
        anchors.left: buttonGenerateCatastrophe.left
        anchors.right: buttonGenerateCatastrophe.right
        anchors.topMargin: mainBunkerPage.buttonMargins
        height: parent.height/10
        buttonText: "Назад"
        onBackButtonReleased: {
            root.backButtonReleased();
        }
    }

    FooterImage {
        id: bunkerSoloGameFooterImage
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 1
        height: parent.height/12
    }

    onBackButtonReleased: {
        stackView.pop(StackView.Immediate);
    }

    onButtonBunkerGenerateCardReleased: {
        stackView.push(bunkerSoloCardPage, StackView.Immediate);
        settingsButtonVisibility = false;
        soundButtonVisibility = false;
        window.cardText = gen.generateCard();
        bunkerSoloCardPage.forceActiveFocus();
    }

    onButtonBunkerGenerateCatastropheReleased: {
        stackView.push(bunkerSoloCatastrophePage, StackView.Immediate);
        settingsButtonVisibility = false;
        soundButtonVisibility = false;
        window.catastropheText = gen.generateCatastrophe();
        bunkerSoloCatastrophePage.forceActiveFocus();
    }
}
