import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.12

Rectangle{
    id: buttonMafiaBack
    signal buttonMafiaBackReleased();
    color: mafiaBackButtonMouseArea.containsPress? "#161616" : mainPage.buttonMafiaColor
    border.width: 2
    border.color: "#333333"
    radius: 15
//    Image {
//        id: playMafiaIcon
//        property int iconMargin: 3
//        width: parent.height - 3
//        height: parent.height - 3
//        anchors.left: parent.left
//        anchors.leftMargin: iconMargin
//        anchors.verticalCenter: parent.verticalCenter
//        source: "images/mafiaIcon.png"
//    }
    Text {
        id: mafiaBackButtonText
        color: "#CC0000"
        FontLoader{
            id: fontLoader
            source: "fonts/Maler.ttf"
        }
        font.pointSize: 14
        anchors.centerIn: parent
        //anchors.horizontalCenterOffset: 10
        
        text: qsTr("Назад")
        font.family: fontLoader.name
    }
    MouseArea{
        id: mafiaBackButtonMouseArea
        anchors.fill: parent
        hoverEnabled: true
        onReleased: {
            buttonMafiaBack.buttonMafiaBackReleased();
        }
    }
}
