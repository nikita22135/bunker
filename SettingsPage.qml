import QtQml.Models 2.2
import QtQml 2.2
import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.12
import QtGraphicalEffects 1.0

SomePage {
    id: root
    backgroundColor: Qt.rgba(0,0,0,0.7)
    signal backSettingsButtonClicked();
    signal avatarClicked(int avatarNumber);
    //focusPolicy is used to change focus from input by clicking
    focusPolicy: Qt.ClickFocus
    Image {
        id: settingsPageBackground
        anchors.fill: parent
        source: "images/mainBackground.jpg"
    }
    BackButtonSettings {
        id: backSettingsButton
        anchors.left: parent.left
        anchors.leftMargin: 7
        anchors.top: parent.top
        anchors.topMargin: 7
        width: 30
        height: 20
    }
    TextField {
        id: nicknameField
        objectName: "nicknameField"
        anchors.top: backSettingsButton.bottom
        anchors.topMargin: 20
        anchors.left: parent.left
        anchors.leftMargin: 7
        width: parent.width - 14
        height: 40
        font.family: fontLoaderMaler.name
        font.pointSize: 12
        color: "#FFCC33"
        text: nickname

        background: Rectangle {
            color: nicknameField.focus ? "#AAAAAA" : "#515151"
            border.color: "#353535"
            border.width: 1
            radius: 7
        }
        onAccepted: {
            //I can't just disable focus, so I pass it to smth else
            backSettingsButton.forceActiveFocus();
        }
    }
    GridLayout {
        id: avatarLayout
        anchors.top: nicknameField.bottom
        anchors.topMargin: 20
        anchors.left: parent.left
        anchors.leftMargin: 7
        width: parent.width - 14
        height: avatarLayout.width
        rows: 3
        columns: 3
        Rectangle {
            id: avatar1
            color: "transparent"
            border.color: (selectedAvatar == 1) ? "#FFCC33" : "#353535"
            border.width: 2
            radius: 7
            Layout.fillHeight: true
            Layout.fillWidth: true
            Layout.row: 1
            Layout.column: 1
            Image {
                id: imageOriginal
                anchors.fill: parent
                source: "images/avatar2.png"
                visible: false
            }
            Rectangle {
                id: rectangleMask
                anchors.fill: parent
                radius: 7
                visible: false
            }
            OpacityMask {
                z: -1
                anchors.fill: imageOriginal
                source: imageOriginal
                maskSource: rectangleMask
            }
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    settingsPage.avatarClicked(1);
                }
            }
        }
        Rectangle {
            id: avatar2
            color: "transparent"
            border.color: (selectedAvatar == 2) ? "#FFCC33" : "#353535"
            border.width: 2
            radius: 7
            Layout.fillHeight: true
            Layout.fillWidth: true
            Layout.row: 1
            Layout.column: 2
            Image {
                id: imageOriginal2
                anchors.fill: parent
                source: "images/avatar2.png"
                visible: false
            }
            Rectangle {
                id: rectangleMask2
                anchors.fill: parent
                radius: 7
                visible: false
            }
            OpacityMask {
                z: -1
                anchors.fill: imageOriginal2
                source: imageOriginal2
                maskSource: rectangleMask2
            }
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    settingsPage.avatarClicked(2);
                }
            }
        }
        Rectangle {
            id: avatar3
            color: "transparent"
            border.color: (selectedAvatar == 3) ? "#FFCC33" : "#353535"
            border.width: 2
            radius: 7
            Layout.fillHeight: true
            Layout.fillWidth: true
            Layout.row: 1
            Layout.column: 3
            Image {
                id: imageOriginal3
                anchors.fill: parent
                source: "images/avatar3.png"
                visible: false
            }
            Rectangle {
                id: rectangleMask3
                anchors.fill: parent
                radius: 7
                visible: false
            }
            OpacityMask {
                z: -1
                anchors.fill: imageOriginal3
                source: imageOriginal3
                maskSource: rectangleMask3
            }
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    settingsPage.avatarClicked(3);
                }
            }
        }
        Rectangle {
            id: avatar4
            color: "transparent"
            border.color: (selectedAvatar == 4) ? "#FFCC33" : "#353535"
            border.width: 2
            radius: 7
            Layout.fillHeight: true
            Layout.fillWidth: true
            Layout.row: 2
            Layout.column: 1
            Image {
                id: imageOriginal4
                anchors.fill: parent
                source: "images/avatar4.png"
                visible: false
            }
            Rectangle {
                id: rectangleMask4
                anchors.fill: parent
                radius: 7
                visible: false
            }
            OpacityMask {
                z: -1
                anchors.fill: imageOriginal4
                source: imageOriginal4
                maskSource: rectangleMask4
            }
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    settingsPage.avatarClicked(4);
                }
            }
        }
        Rectangle {
            id: avatar5
            color: "transparent"
            border.color: (selectedAvatar == 5) ? "#FFCC33" : "#353535"
            border.width: 2
            radius: 7
            Layout.fillHeight: true
            Layout.fillWidth: true
            Layout.row: 2
            Layout.column: 2
            Image {
                id: imageOriginal5
                anchors.fill: parent
                source: "images/avatar5.png"
                visible: false
            }
            Rectangle {
                id: rectangleMask5
                anchors.fill: parent
                radius: 7
                visible: false
            }
            OpacityMask {
                z: -1
                anchors.fill: imageOriginal5
                source: imageOriginal5
                maskSource: rectangleMask5
            }
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    settingsPage.avatarClicked(5);
                }
            }
        }
        Rectangle {
            id: avatar6
            color: "transparent"
            border.color: (selectedAvatar == 6) ? "#FFCC33" : "#353535"
            border.width: 2
            radius: 7
            Layout.fillHeight: true
            Layout.fillWidth: true
            Layout.row: 2
            Layout.column: 3
            Image {
                id: imageOriginal6
                anchors.fill: parent
                source: "images/avatar6.png"
                visible: false
            }
            Rectangle {
                id: rectangleMask6
                anchors.fill: parent
                radius: 7
                visible: false
            }
            OpacityMask {
                z: -1
                anchors.fill: imageOriginal6
                source: imageOriginal6
                maskSource: rectangleMask6
            }
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    settingsPage.avatarClicked(6);
                }
            }
        }
        Rectangle {
            id: avatar7
            color: "transparent"
            border.color: (selectedAvatar == 7) ? "#FFCC33" : "#353535"
            border.width: 2
            radius: 7
            Layout.fillHeight: true
            Layout.fillWidth: true
            Layout.row: 3
            Layout.column: 1
            Image {
                id: imageOriginal7
                anchors.fill: parent
                source: "images/avatar7.png"
                visible: false
            }
            Rectangle {
                id: rectangleMask7
                anchors.fill: parent
                radius: 7
                visible: false
            }
            OpacityMask {
                z: -1
                anchors.fill: imageOriginal7
                source: imageOriginal7
                maskSource: rectangleMask7
            }
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    settingsPage.avatarClicked(7);
                }
            }
        }
        Rectangle {
            id: avatar8
            color: "transparent"
            border.color: (selectedAvatar == 8) ? "#FFCC33" : "#353535"
            border.width: 2
            radius: 7
            Layout.fillHeight: true
            Layout.fillWidth: true
            Layout.row: 3
            Layout.column: 2
            Image {
                id: imageOriginal8
                anchors.fill: parent
                source: "images/avatar8.png"
                visible: false
            }
            Rectangle {
                id: rectangleMask8
                anchors.fill: parent
                radius: 7
                visible: false
            }
            OpacityMask {
                z: -1
                anchors.fill: imageOriginal8
                source: imageOriginal8
                maskSource: rectangleMask8
            }
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    settingsPage.avatarClicked(8);
                }
            }
        }
        Rectangle {
            id: avatar9
            color: "transparent"
            border.color: (selectedAvatar == 9) ? "#FFCC33" : "#353535"
            border.width: 2
            radius: 7
            Layout.fillHeight: true
            Layout.fillWidth: true
            Layout.row: 3
            Layout.column: 3
            Image {
                id: imageOriginal9
                anchors.fill: parent
                source: "images/avatar9.png"
                visible: false
            }
            Rectangle {
                id: rectangleMask9
                anchors.fill: parent
                radius: 7
                visible: false
            }
            OpacityMask {
                z: -1
                anchors.fill: imageOriginal9
                source: imageOriginal9
                maskSource: rectangleMask9
            }
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    settingsPage.avatarClicked(9);
                }
            }
        }
    }
    onBackSettingsButtonClicked: {
        backSettingsButton.forceActiveFocus();
        settings.setNickname(nicknameField.text);
        nickname = nicknameField.text;
        stackView.pop(StackView.Immediate);
        settingsButtonVisibility = true;
    }
    onAvatarClicked: {
        selectedAvatar = avatarNumber;
        settings.setAvatarNumber(avatarNumber);
    }
}
