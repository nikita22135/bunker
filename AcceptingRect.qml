import QtQml.Models 2.2
import QtQml 2.2
import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.12
import QtGraphicalEffects 1.0

Rectangle{
    id: acceptingRect
    property alias rectHeight: acceptingRect.height
    property alias titleText: acceptingText.text
    width: parent.width/1.1
    anchors.centerIn: parent
    height: parent.height/1.1
    color: Qt.rgba(0,0,0,0.75);
    radius: 5
    visible: false
    z: 10
    signal accepted();
    FontLoader{
        id: fontLoader
        source: "fonts/Roboto-Regular.ttf"
    }
    Text {
        id: acceptingText
        color: "#FFCC33"
        text: qsTr("Раскрыть характеристику?")
        font.family: fontLoader.name
        width: parent.width
        height: parent.height/3
        anchors.top: parent.top
        anchors.topMargin: 2
        horizontalAlignment: Text.AlignHCenter
        anchors.horizontalCenter: parent.horizontalCenter
        font.pointSize: 12
    }
    Rectangle{
        id: acceptButton
        anchors.left: parent.left
        anchors.top: acceptingText.bottom
        anchors.bottom: parent.bottom
        anchors.topMargin: 2
        anchors.leftMargin: 10
        width: parent.width/3.5
        color: "#BEF73E"
        radius: 7
        border.width: 2
        border.color: mainBunkerPage.buttonBorderColor
        Text {
            id: acceptButtonText
            anchors.centerIn: parent
            text: qsTr("ДА")
            font.family: fontLoader.name
            font.pointSize: 12
        }
        MouseArea{
            anchors.fill: parent
            onReleased: {
                acceptingRect.visible = false;
                acceptingRect.accepted();
            }
        }
    }
    Rectangle{
        id: declineButton
        anchors.right: parent.right
        anchors.top: acceptingText.bottom
        anchors.bottom: parent.bottom
        anchors.topMargin: 2
        anchors.rightMargin: 10
        width: parent.width/3.5
        color: Qt.rgba(170,0,0,0.95)
        radius: 7
        border.width: 2
        border.color: mainBunkerPage.buttonBorderColor
        Text {
            id: declineButtonText
            anchors.centerIn:parent
            text: qsTr("НЕТ")
            font.family: fontLoader.name
            font.pointSize: 12
        }
        MouseArea{
            anchors.fill: parent
            onReleased: {
                acceptingRect.visible = false;
            }
        }
    }
}
