import QtQml.Models 2.2
import QtQml 2.2
import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.12

SomePage {
    id: root
    signal nicknamesChanged();
    signal startGameBunkerButtonReleased();
    property int counter: 0
    property alias startGameBunkerButtonVisibility: startGameBunkerButton.visible
    property alias tooFewPlayersToStartBunkerGameMessageVisibility: tooFewPlayersToStartBunkerGameMessage.visible
    Image {
        id: bunkerLobbyBackground
        source: "images/mainBackground.jpg"
        anchors.fill: parent
    }
    ListView{
        id: bunkerNicknamesListView
        anchors.fill: parent
        delegate:
            Rectangle{
            width: parent.width
            height: 45
            color: "#515151"
            border.width: 1
            border.color: "#202020"
            radius: 5
            Text {
                text: modelData
                font.pointSize: 12
                font.bold: true
                font.family: fontLoaderMaler.name
                color: "#FFCC33"
                anchors.centerIn: parent
            }
        }
        model: BunkerNicknamesListModel { id: bunkerHostListModel }
    }

    BunkerNicknamesListModel {
        id: bunkerNicknamesListModel
    }

    onNicknamesChanged: {
        bunkerNicknamesListModel.nicknamesChanged();
        root.counter = 0;
        bunkerHostListModel.clear();
        for (var i = 0; i<16; i++)
        {
            if (nicknames[i])
            {
                root.counter++;
                startGameBunkerButton.startBunkerGameText = counter+"/16 "+"Начать игру";
                bunkerHostListModel.append(
                            {
                                text: nicknames[i]
                            })
            }
        }
    }

    StartGameBunkerButton {
        id: startGameBunkerButton
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 1
        anchors.left: parent.left
        anchors.right: parent.right
        height: 45
        onStartGameBunkerButtonReleased:{
            root.startGameBunkerButtonReleased();
        }
    }

    onStartGameBunkerButtonReleased: {
        client.writeData("startBunkerGame\r\n");
        window.catastropheText = gen.sendCatastropheParameters();
    }
    Rectangle{
        id: tooFewPlayersToStartBunkerGameMessage
        anchors.fill: parent;
        color: Qt.rgba(20,20,20,0.5);
        visible: false
        Text {
            id: tooFewPlayersToStartBunkerGameMessageText
            text: qsTr("Необходимо не менее 4х игроков, чтобы начать игру");
            font.bold: true
            font.pointSize: 16
            anchors.centerIn: parent
            width: parent.width - 10
            wrapMode: Text.WordWrap
            font.family: fontLoaderMaler.name
        }
        Rectangle{
            id: okButton
            signal okButtonReleased();
            anchors.bottom: parent.bottom
            anchors.horizontalCenter: parent.horizontalCenter
            width: parent.width/1.4
            height: parent.height/10
            anchors.bottomMargin: 7
            color: okButtonMouseArea.containsPress? Qt.darker(mainBunkerPage.buttonColor, 1.1) : mainBunkerPage.buttonColor
            radius: 15
            border.width: 2
            border.color: mainBunkerPage.buttonBorderColor

            Text {
                id: okButtonText
                font.family: fontLoaderMaler.name
                text: qsTr("ОК")
                anchors.centerIn: parent
                font.pointSize: 14
            }
            MouseArea{
                id: okButtonMouseArea
                anchors.fill: parent
                hoverEnabled: true
                onReleased: {
                    okButton.okButtonReleased();
                }
            }
            onOkButtonReleased: {
                root.tooFewPlayersToStartBunkerGameMessageVisibility = false;
            }
        }
    }
}
