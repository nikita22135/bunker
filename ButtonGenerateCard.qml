import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.12

Rectangle{
    id: buttonGenerateCard
    color: buttonGenerateCardMouseArea.containsPress? Qt.darker(mainBunkerPage.buttonColor, 1.1) : mainBunkerPage.buttonColor
    radius: 15
    border.width: 2
    border.color: mainBunkerPage.buttonBorderColor
    signal buttonBunkerGenerateCardReleased();
    Text {
        id: generateCardText
        FontLoader{
            id: fontLoader
            source: "fonts/Maler.ttf"
        }
        font.family: fontLoader.name
        
        text: qsTr("Сгенерировать\nкарточку")
        horizontalAlignment: Text.AlignHCenter
        anchors.centerIn: parent
        font.letterSpacing: 0.15
        font.pointSize: 14
    }

    MouseArea{
        id: buttonGenerateCardMouseArea
        anchors.fill: parent
        hoverEnabled: true
        onReleased: {
            buttonGenerateCard.buttonBunkerGenerateCardReleased();
        }
    }
}
