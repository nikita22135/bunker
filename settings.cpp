#include "settings.h"
#include "qdebug.h"

#include "QSettings"

Settings::Settings(QObject* parent) : QObject(parent)
{
    nickname = "Игрок";
    isMuted = false;
    avatarNumber = 1;
    QSettings settings("Settings.ini", QSettings::IniFormat);
    settings.setIniCodec("UTF-8");

    if (settings.value("nickname").isNull())
    {
        settings.setValue("nickname", nickname);
    }
    else
    {
        nickname = settings.value("nickname").toString();
    }

    if (settings.value("isMuted").isNull())
    {
        settings.setValue("isMuted", isMuted);
    }
    else
    {
        isMuted = settings.value("isMuted").toBool();
    }

    if (settings.value("avatarNumber").isNull())
    {
        settings.setValue("avatarNumber", avatarNumber);
    }
    else
    {
        avatarNumber = settings.value("avatarNumber").toInt();
    }

    muteSound();
}

QString Settings::getNickname() const { return nickname; }

void Settings::setNickname(const QString& value)
{
    nickname = value;
    QSettings settings("Settings.ini", QSettings::IniFormat);
    settings.setIniCodec("UTF-8");
    settings.setValue("nickname", nickname);
}

bool Settings::getMuted() const { return isMuted; }

void Settings::setMuted(const bool& value)
{
    isMuted = value;
    QSettings settings("Settings.ini", QSettings::IniFormat);
    settings.setIniCodec("UTF-8");
    settings.setValue("isMuted", isMuted);
    muteSound();
}

void Settings::muteSound()
{
    if (isMuted)
    {
        //will mute sound
    }
}

int Settings::getAvatarNumber() const { return avatarNumber; }

void Settings::setAvatarNumber(int value)
{
    avatarNumber = value;
    QSettings settings("Settings.ini", QSettings::IniFormat);
    settings.setIniCodec("UTF-8");
    settings.setValue("avatarNumber", avatarNumber);
}
