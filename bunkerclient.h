#ifndef BUNKERCLIENT_H
#define BUNKERCLIENT_H

#include <QObject>
#include <QtNetwork/QTcpSocket>

class BunkerClient : public QObject
{
    Q_OBJECT
public:
    explicit BunkerClient(QObject *parent = nullptr);

signals:
    void hostBunkerApproved();
    void nicknamesListUpdated(QStringList nicknamesList);
    void bunkerLobbyHostsListUpdated(QStringList bunkerHosts);
    void bunkerLobbyHostsIdsListUpdated(QList<int> bunkerHostsIds);
    void bunkerPlayersCountUpdated(QList<int> bunkerPlayersCount);
    void bunkerIsPassUpdated(QList<bool> bunkerIsPass);
    void idCreated();
    void connectionToLobbyApproved();
    void gameIsNotStarted();
    void startBunkerGame();
    void sendCatastropheParameters(int catastropheId, QList<int> itemsList, int electricity, int peopleCount);
    void notEnoughSpace();
    void passIsNotMatch();
    void biologyComes(QString bio, int num);
    void professionComes(QString prof, int num);
    void healthComes(QString heal, int num);
    void characterComes(QString charact, int num);
    void hobbieComes(QString hob, int num);
    void phobiaComes(QString phob, int num);
    void bagComes(QString bg, int num);
    void special1Comes(QString spcial1, int num);
    void changeHealthToRandom(int playerNum);
public:
    Q_INVOKABLE void connectToServer();
    Q_INVOKABLE void writeData(QString data);
    Q_INVOKABLE int getId();
public slots:
    void sendCatastropheParametersSlot(int catastropheId, QList<int> itemsList, int electricity, int peopleCount);
private:
    QTcpSocket* _socket;
    QStringList _nicknamesList;
    int id;
private slots:
    void onConnected();
    void readData();

};

#endif // BUNKERCLIENT_H
