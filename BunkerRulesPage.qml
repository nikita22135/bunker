import QtQml.Models 2.2
import QtQml 2.2
import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.12
import Qt.labs.qmlmodels 1.0

SomePage {
    id: root
    signal backButtonClicked();
    backgroundColor: Qt.rgba(0,0,0,0.7)

    ListModel {
        id: listModel
        ListElement {
            type: "header"
            text: "Сюжет игры"
        }
        ListElement {
            type: "rules"
            text: "На земле произошла катастрофа. Таких катастроф семь видов. Каждая из них описывает состояние планеты и количество выживших. Персонажи состоят из семи характеристик: профессия, здоровье и другая информация. У каждого игрока две цели. Личная – попасть в бункер, чтобы возрождать планету. Командная — проследить, чтобы в бункер попали только здоровые и пригодные к выживанию люди. В ходе игры вы постепенно раскрываете своего персонажа, показываете сильные карты и объясняете, почему именно вы должны выжить. Можете менять ход игры: используйте смены карт, перераздачи и иммунитеты. Игра отлично развивает дискуссионные навыки и навыки убеждения. голосование. На основании голосования игроки должны выбрать, кто из них попадёт в спасительный бункер. Чтобы определить, кто из героев попадет в бункер, необходимо выбрать самых полезных для восстановления жизни на постапокалиптической планете. Группа выживших будет возрождать быт и население Земли. Оружия и насилия нет, только дискуссия и обоснование своей важности и необходимости."
        }
        ListElement {
            type: "header"
            text: "Справка"
        }
        ListElement {
            type: "reference"
            image: "sexIcon"
            text: "Пол и возраст"
        }
        ListElement {
            type: "reference"
            image: "professionIcon"
            text: "Профессия"
        }
        ListElement {
            type: "reference"
            image: "healthIcon"
            text: "Здоровье"
        }
        ListElement {
            type: "reference"
            image: "characterIcon"
            text: "Характер"
        }
        ListElement {
            type: "reference"
            image: "hobbiesIcon"
            text: "Хобби"
        }
        ListElement {
            type: "reference"
            image: "phobiaIcon"
            text: "Фобия"
        }
        ListElement {
            type: "reference"
            image: "bagIcon"
            text: "Предметы"
        }
        ListElement {
            type: "reference"
            image: "specialsIcon"
            text: "Особое свойство"
        }
    }

    ListView {
        id: listView
        anchors.fill: parent
        clip: true
        model: listModel
        delegate: delegateChooser
        spacing: 1

        DelegateChooser {
            id: delegateChooser
            role: "type"
            DelegateChoice {
                roleValue: "header"
                delegate: headerDelegateComponent
            }
            DelegateChoice {
                roleValue: "rules"
                delegate: rulesDelegateComponent
            }
            DelegateChoice {
                roleValue: "reference"
                delegate: referenceDelegateComponent
            }
        }
    }

    Component {
        id: headerDelegateComponent
        Rectangle {
            width: parent.width
            height: 40
            color: "transparent"
            Text {
                anchors.centerIn: parent
                color: "#FFFFFF"
                wrapMode: Text.WordWrap
                font.pointSize: 14
                text: model.text
            }
        }
    }

    Component {
        id: rulesDelegateComponent
        Text {
            anchors.right: parent.right
            anchors.rightMargin: 6
            anchors.left: parent.left
            anchors.leftMargin: 6
            font.letterSpacing: 0.05
            color: "#FFFFFF"
            wrapMode: Text.WordWrap
            font.pointSize: 12
            text: model.text
        }
    }

    Component {
        id: referenceDelegateComponent
        Rectangle {
            width: listView.width
            height: 55
            color: "gray"
            radius: 5
            border.width: 2
            border.color: "#202020"
            Rectangle{
                id: r1
                anchors.left: parent.left
                anchors.leftMargin: 3
                anchors.top: parent.top
                anchors.topMargin: 3
                height: parent.height - 6
                width: parent.height - 6
                radius: parent.height/2
                border.color: "#202020"
                border.width: 1
                color: "#909090"
                Image {
                    source: "images/" + model.image + ".png"
                    anchors.fill: r1
                    anchors.margins: 7
                }
            }
            Text {
                font.letterSpacing: 0.2
                color: "#FFFFFF"
                wrapMode: Text.WordWrap
                font.pointSize: 12
                text: model.text
                //font.family: fontLoaderMaler.name
                anchors.left: r1.right
                anchors.leftMargin: 20
                anchors.right: parent.right
                anchors.rightMargin: 6
                anchors.top: parent.top
                anchors.bottom: parent.bottom
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignLeft
            }
        }
    }

    Keys.onPressed: {
        if (event.key == Qt.Key_Escape) {
            stackView.pop(StackView.Immediate);
            settingsButtonVisibility = true;
            soundButtonVisibility = true;
            event.accepted = true;
        }
        if (event.key == Qt.Key_Back)
        {
            stackView.pop(StackView.Immediate);
            settingsButtonVisibility = true;
            soundButtonVisibility = true;
            event.accepted = true;
        }
    }
}
